package com.incred.janus.repositories;

import com.incred.janus.models.core.ApplicationConfiguration;
import com.incred.janus.models.core.SortingAndOrderArguments;
import com.incred.janus.models.core.TransactionLog;
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.spring.tx.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Singleton
public class TransactionLogRepositoryImpl implements TransactionLogRepository {

    private final static List<String> VALID_PROPERTY_NAMES = Arrays
            .asList("id", "name", "vendor", "created_at", "updated_at", "platform", "sender");
    private final ApplicationConfiguration applicationConfiguration;
    @PersistenceContext
    EntityManager entityManager;

    @Inject
    public TransactionLogRepositoryImpl(@CurrentSession EntityManager entityManager,
                                        ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TransactionLog> findById(@NotNull Long id) {
        return Optional.ofNullable(entityManager.find(TransactionLog.class, id));
    }

    @Override
    @Transactional
    public TransactionLog save(@NotBlank TransactionLog log) {
        entityManager.persist(log);
        return log;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TransactionLog> findAll(@NotNull SortingAndOrderArguments args) {
        String qlString = "SELECT l FROM transaction_log as l";
        if (args.getOrder().isPresent() && args.getSort().isPresent() && VALID_PROPERTY_NAMES
                .contains(args.getSort().get())) {
            qlString += " ORDER BY l." + args.getSort().get() + " " + args.getOrder().get().toLowerCase();
        }
        TypedQuery<TransactionLog> query = entityManager.createQuery(qlString, TransactionLog.class);
        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getMax));
        args.getOffset().ifPresent(query::setFirstResult);

        return query.getResultList();
    }

    @Override
    @Transactional
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(log -> entityManager.remove(log));
    }

    @Override
    @Transactional
    public TransactionLog update(@NotNull Long id, @NotBlank TransactionLog log) {
        return entityManager.merge(log);
    }
}

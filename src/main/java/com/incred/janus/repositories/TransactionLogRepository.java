package com.incred.janus.repositories;

import com.incred.janus.models.core.SortingAndOrderArguments;
import com.incred.janus.models.core.TransactionLog;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface TransactionLogRepository {

    Optional<TransactionLog> findById(@NotNull Long id);

    TransactionLog save(@NotBlank TransactionLog name);

    List<TransactionLog> findAll(@NotNull SortingAndOrderArguments args);

    void deleteById(@NotNull Long id);

    @NotBlank TransactionLog update(@NotNull Long id, @NotBlank TransactionLog log);
}
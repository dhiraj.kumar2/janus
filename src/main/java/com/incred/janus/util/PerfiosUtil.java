package com.incred.janus.util;

import com.incred.janus.models.perfios.client.PayloadRequest;
import com.incred.janus.models.perfios.client.PerfiosRequest;
import com.incred.janus.models.perfios.response.PerfiosError;
import com.incred.janus.models.perfios.response.SOAPResponse;
import com.incred.janus.models.perfios.response.UploadStmt;
import com.incred.janus.models.perfios.vendor.Operation;
import com.incred.janus.models.perfios.vendor.PerfiosVendorRequest;
import com.incred.janus.services.PerfiosService;
import io.micrometer.core.instrument.Timer;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.HttpStatus;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Collections;

@Singleton
@Slf4j
public class PerfiosUtil {

    @Value("${perfios.url}")
    private String url;
    @Value("${perfios.vendor}")
    private String vendor;
    @Value("${perfios.callbackUrl}")
    private String callbackUrl;

    private static RSAPublicKey publicKey = null;
    private static RSAPrivateKey privateKey = null;

    private static final String ACCEPTANCE_POLICY = "atLeastOneTransactionInRange";

    public static final String VENDOR_PERFIOS = "PERFIOS";
    public static final String BANK_TRANSCRIPTION = "BANK_TRANSCRIPTION";
    public static final String PERFIOS = "PERFIOS";

    private static final String UNSUPPORTED_TYPE = "UnsupportedFileType";
    private static final String CANNOT_PROCESS_FILE = "CannotProcessFile";
    private static final String TRANSACTION_NOT_FOUND = "TransactionNotFound";
    private static final String METHOD_NOT_ALLOWED = "MethodNotAllowed";
    private static final String INTERNAL_SERVER_ERROR = "InternalError";

    private static final String E_STATEMENT_UNSUPPORTED_FORMAT = "E_STATEMENT_UNSUPPORTED_FORMAT";
    private static final String E_OTHER = "E_OTHER";

    private static synchronized void initKeys() {
        try {
            if (privateKey == null) {
                privateKey = RSAUtil.getPrivateKey("perfios/private.key");
            }
            if (publicKey == null) {
                RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(privateKey.getModulus(),
                        privateKey.getPrivateExponent());
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                publicKey = (RSAPublicKey) keyFactory.generatePublic(publicKeySpec);
            }
        } catch (IOException | GeneralSecurityException e) {
            log.error("error {}", ExceptionUtils.getStackTrace(e));
        }
    }

    public static PerfiosError getError(UploadStmt uploadStmt) {
        PerfiosError error = new PerfiosError();

        if (uploadStmt != null) {
            switch (uploadStmt.getStatusCode()) {
                case 415:
                    error.setCode(UNSUPPORTED_TYPE);
                    error.setStatementErrorCode(E_STATEMENT_UNSUPPORTED_FORMAT);
                    break;
                case 400:
                    error.setCode(CANNOT_PROCESS_FILE);
                    error.setStatementErrorCode(E_OTHER);
                    break;
                case 404:
                    error.setCode(TRANSACTION_NOT_FOUND);
                    error.setStatementErrorCode(E_OTHER);
                    break;
                case 405:
                    error.setCode(METHOD_NOT_ALLOWED);
                    error.setStatementErrorCode(E_OTHER);
                    break;
                default:
                    error.setCode(INTERNAL_SERVER_ERROR);
                    error.setStatementErrorCode(E_OTHER);
                    break;
            }

            error.setMessage(uploadStmt.getMessage());
        }

        return error;
    }

    public static String getSignature(String payload) {
        try {
            String hexDigest = DigestUtils.sha1Hex(payload);
            return RSAUtil.privateEncrypt(hexDigest, privateKey);
        } catch (GeneralSecurityException | IOException e) {
            log.error("error {}", ExceptionUtils.getStackTrace(e));
        }
        return "";
    }

    public String getPath(@NotNull Operation operation) {
        String path = "";
        switch (operation) {
            case START:
                path = "api/statement/start";
                break;
            case UPLOAD:
                path = "api/statement/upload";
                break;
            case UPLOAD_COMPLETE:
                path = "api/statement/done";
                break;
            case CHECK_STATUS:
                path = "txnstatus";
                break;
            case RETRIEVE_DATA:
                path = "retrieve";
                break;
            case INSTITUTIONS:
                path = "institutions";
                break;
        }
        return url + path;
    }

    public PerfiosVendorRequest getPerfiosVendorReq(PayloadRequest request) {
        if (privateKey == null) {
            initKeys();
        }

        StringBuilder payload = new StringBuilder().append("<payload>").append("<apiVersion>2.1</apiVersion>")
                .append("<vendorId>").append(request.getVendorId()).append("</vendorId>");

        switch (request.getOperation()) {
            case START:
                payload.append("<txnId>").append(request.getClientTxnId()).append("</txnId>")
                        .append("<institutionId>").append(request.getPerfiosCode()).append("</institutionId>")
                        .append("<loanAmount>").append(request.getLoanAmount()).append("</loanAmount>")
                        .append("<loanDuration>").append(request.getLoanDuration()).append("</loanDuration>")
                        .append("<loanType>").append(request.getLoanType()).append("</loanType>")
                        .append("<acceptancePolicy>").append(ACCEPTANCE_POLICY).append("</acceptancePolicy>")
                        .append("<transactionCompleteCallbackUrl>").append(request.getCallbackUrl())
                        .append("</transactionCompleteCallbackUrl>");
                if (request.isScannedStmts()) {
                    payload.append("<yearMonthFrom>").append(request.getYearMonthFrom()).append("</yearMonthFrom>")
                            .append("<yearMonthTo>").append(request.getYearMonthTo()).append("</yearMonthTo>")
                            .append("<uploadingScannedStatements>true</uploadingScannedStatements>");
                } else {
                    payload.append("<uploadingScannedStatements>false</uploadingScannedStatements>");
                }
                break;
            case UPLOAD_COMPLETE:
                payload.append("<perfiosTransactionId>").append(request.getPerfiosTxnId()).append("</perfiosTransactionId>");
                break;
            case CHECK_STATUS:
                payload.append("<txnId>").append(request.getClientTxnId()).append("</txnId>");
                break;
            case RETRIEVE_DATA:
                payload.append("<txnId>").append(request.getClientTxnId()).append("</txnId>")
                        .append("<perfiosTransactionId>").append(request.getPerfiosTxnId()).append("</perfiosTransactionId>")
                        .append("<reportType>json</reportType>");
                break;
            case INSTITUTIONS:
                payload.append("<destination>statement</destination>");
                break;
        }
        payload.append("</payload>");

        return PerfiosVendorRequest.builder()
                .payload(payload.toString())
                .signature(getSignature(payload.toString()))
                .clientTxnId(request.getClientTxnId()).build();
    }

    public <T> T toPOJO(String xml, Class<T> type) throws Exception {
        InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        JAXBContext jaxbContext = JAXBContext.newInstance(type);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader someSource = factory.createXMLEventReader(stream);
        JAXBElement<T> userElement = jaxbUnmarshaller.unmarshal(someSource, type);
        return userElement.getValue();
    }

    public Mono<SOAPResponse> getSOAPResponse(PayloadRequest request, SoapUtil soapUtil) {
        try {
            PerfiosVendorRequest perfiosVendorReq = getPerfiosVendorReq(request);
            return Mono.just(soapUtil.send(getPath(request.getOperation()),
                    Collections.singletonMap(PerfiosService.CONTENT_TYPE, PerfiosService.FORM_URLENCODED), perfiosVendorReq.queryString()))
                    .subscribeOn(Schedulers.elastic());
        } catch (Exception e) {
            log.error("error sending soap request {} {}", request.getClientTxnId(), ExceptionUtils.getStackTrace(e));
            return Mono.empty();
        }
    }

    public UploadStmt upload(String perfiosTxnId, String key, S3Util s3Util, String password, String bucket) {
        Response response = null;
        try {
            MetricsUtil.init();
            Timer.Sample sample = Timer.start(MetricsUtil.registry);
            log.info("downloading from s3 {} {} {}", key, System.currentTimeMillis(), perfiosTxnId);

            File file = s3Util.get(bucket, key);
            log.info("downloaded from s3 {} {} {}", key, System.currentTimeMillis(), perfiosTxnId);

            response = MultipartUtil.okHttp(perfiosTxnId, file, vendor, getPath(Operation.UPLOAD), password);
            log.info("uploaded to perfios {} {} {}", key, System.currentTimeMillis(), perfiosTxnId);

            sample.stop(MetricsUtil.registry.timer("bank_transcription.upload.timer", "vendor", PERFIOS));
            return UploadStmt.builder().statusCode(response.code()).message(response.message()).file(file).build();
        } catch (Exception e) {
            log.error("error in uploading statement {} {}", perfiosTxnId, ExceptionUtils.getStackTrace(e));
            return UploadStmt.builder().statusCode(HttpStatus.INTERNAL_SERVER_ERROR.getCode()).build();
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    public PayloadRequest getPayloadRequest(Operation operation, String perfiosTxnId, String txnId) {
        return PayloadRequest.builder()
                .operation(operation)
                .perfiosTxnId(perfiosTxnId)
                .clientTxnId(txnId)
                .callbackUrl(callbackUrl)
                .vendorId(vendor)
                .build();
    }

    public PayloadRequest getStartPerfiosRequest(PerfiosRequest perfiosReq, int perfiosCode) {
        return PayloadRequest.builder()
                .operation(Operation.START)
                .loanType(perfiosReq.getLoanType())
                .loanAmount(perfiosReq.getLoanAmount())
                .loanDuration(perfiosReq.getLoanDuration())
                .callbackUrl(callbackUrl)
                .vendorId(vendor)
                .perfiosCode(perfiosCode)
                .clientTxnId(perfiosReq.getTransactionId())
                .scannedStmts(perfiosReq.isScannedStmts())
                .yearMonthFrom(perfiosReq.getYearMonthFrom())
                .yearMonthTo(perfiosReq.getYearMonthTo())
                .build();
    }
}

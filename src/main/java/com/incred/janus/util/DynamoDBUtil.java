package com.incred.janus.util;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.context.annotation.Value;
import io.micronaut.core.util.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;
import java.util.*;
import java.util.Map.Entry;

@Singleton
public class DynamoDBUtil {

    public static String VALIDATION_PAN = "JANUS_VALIDATION_PAN";
    public static String BUREAU = "JANUS_BUREAU";
    public static String HUNTER = "JANUS_HUNTER";
    private final List<String> attributeTypes = new ArrayList<>(Arrays.asList("S", "B", "N"));
    @Value("${aws.dynamo.endpoint}")
    String endpoint;
    @Value("${aws.accessKey}")
    String accessKey;
    @Value("${aws.secretAccessKey}")
    String secretAccessKey;
    @Value("${aws.region}")
    String region;
    @Value("${env}")
    String env;
    private AmazonDynamoDB amazonDynamoDB;
    private DynamoDB dynamoDB;
    private ObjectMapper objectMapper = new ObjectMapper();

    public synchronized void init(String table) throws InterruptedException {
        if (amazonDynamoDB == null) {
            table = getENV(table);
            amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().withCredentials(
                    new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretAccessKey)))
                                                        .withEndpointConfiguration(
                                                                new AwsClientBuilder.EndpointConfiguration(endpoint,
                                                                                                           region))
                                                        .build();
            dynamoDB = new DynamoDB(amazonDynamoDB);
            createTable(table);
        }
    }

    @NotNull
    public String getENV(String table) {
        String envVal = System.getenv("ENV") == null ? env : System.getenv("ENV");
        if (StringUtils.isEmpty(envVal)) {
            envVal = System.getenv("env");
        }
        String env = envVal == null ? "DEV" : envVal.toUpperCase();
        table = env + "." + table;
        return table;
    }

    public void createTable(String table) throws InterruptedException {
        Map<String, String> attributes;
        if (table.contains(VALIDATION_PAN)) {
            attributes = Collections.singletonMap("DOC_NO", "S");
            createTable(table, attributes, new ArrayList<>(Collections.singletonMap("DOC_NO", "S").keySet()));
        }
        if (table.contains(BUREAU)) {
            attributes = Collections.singletonMap("APPLICATION_ID", "S");
            createTable(table, attributes, new ArrayList<>(attributes.keySet()));
        }
        if (table.contains(HUNTER)) {
            attributes = Collections.singletonMap("APPLICATION_ID", "S");
            createTable(table, attributes, new ArrayList<>(attributes.keySet()));
        }
    }

    private void updateTTL() {
        UpdateTimeToLiveRequest req = new UpdateTimeToLiveRequest();
        req.setTableName(VALIDATION_PAN);

        TimeToLiveSpecification ttlSpec = new TimeToLiveSpecification();
        ttlSpec.setAttributeName("TTL");
        ttlSpec.setEnabled(true);
        req.withTimeToLiveSpecification(ttlSpec);
        amazonDynamoDB.updateTimeToLive(req);
    }

    private void createTable(String name, Map<String, String> attributes, List<String> keys) throws InterruptedException {
        boolean tableExists = amazonDynamoDB.listTables().getTableNames().stream()
                                            .anyMatch(tableName -> tableName.equals(name));
        if (tableExists) {
            return;
        }
        CreateTableRequest createTableRequest = new CreateTableRequest();
        createTableRequest.withTableName(name);
        for (int i = 0; i < keys.size(); i++) {
            createTableRequest.withKeySchema(new KeySchemaElement(keys.get(i), i == 0 ? "HASH" : "RANGE"));
        }
        for (Entry<String, String> entry : attributes.entrySet()) {
            assert !attributeTypes.contains(entry.getValue()) : "Unknown attribute type : " + entry.getValue();
            createTableRequest.withAttributeDefinitions(new AttributeDefinition(entry.getKey(), entry.getValue()));
        }
        createTableRequest.withProvisionedThroughput(new ProvisionedThroughput(5L, 5L));
        Table tableResult = dynamoDB.createTable(createTableRequest);
        tableResult.waitForActive();
        updateTTL();
    }

    public void put(String tableName, String key, String value, Map<String, Object> attributes) throws JsonProcessingException, InterruptedException {
        if (amazonDynamoDB == null) {
            init(tableName);
        }
        Table table = dynamoDB.getTable(getENV(tableName));
        table.waitForActive();
        Item item = new Item().withPrimaryKey(key, value);
        for (Entry<String, Object> entry : attributes.entrySet()) {
            String attributeName = entry.getKey();
            Object attributeValue = entry.getValue();
            if (attributeValue instanceof String) {
                item.withString(attributeName, (String) attributeValue);
            } else if (attributeValue instanceof Number) {
                item.withNumber(attributeName, (Number) attributeValue);
            } else if (attributeValue instanceof Boolean) {
                item.withBinary(attributeName, new byte[]{(byte) ((Boolean) attributeValue ? 1 : 0)});
            } else if (attributeValue != null) {
                String json = objectMapper.writeValueAsString(attributeValue);
                item.withJSON(attributeName, json);
            }
        }
        long epoch = new Date().getTime() / 1000L;
        item.withNumber("CREATED_AT", epoch);
        item.withNumber("TTL", epoch + (30L * 86400L));
        table.putItem(item);
    }

    public String get(String tableName, String key, String value) throws InterruptedException {
        if (amazonDynamoDB == null) {
            init(tableName);
        }
        Table table = dynamoDB.getTable(tableName);
        Item item = table.getItem(key, value);
        return item.toJSON();
    }
}

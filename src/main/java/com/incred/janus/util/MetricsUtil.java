package com.incred.janus.util;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;

import javax.inject.Singleton;
import java.util.Map;

@Singleton
public class MetricsUtil {
    public static PrometheusMeterRegistry registry;

    public static void init() {
        if (registry == null) {
            synchronized (MetricsUtil.class) {
                PrometheusMeterRegistry prometheusRegistry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
                prometheusRegistry.config().commonTags("application", "JANUS");
                registry = prometheusRegistry;
            }
        }
    }

    public String scrapeMetrics() {
        if (registry == null) {
            init();
        }
        return registry.scrape();
    }

    public Counter register(String name, String description, Map<String, String> tags) {
        if (registry == null) {
            init();
        }
        Counter.Builder builder = Counter.builder(name);
        tags.forEach(builder::tag);
        builder.description(description);
        return builder.register(registry);
    }

    public MeterRegistry getMeterRegistry() {
    if (registry == null) {
            init();
        }
        return registry;
    }
}

package com.incred.janus.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.incred.janus.models.cibil.core.CIBILRequest;
import com.incred.janus.models.core.Request;
import com.incred.janus.models.core.Response;
import com.incred.janus.models.core.TransactionLog;
import com.incred.janus.models.hunter.client.HunterRequest;
import com.incred.janus.models.perfios.client.PerfiosRequest;
import com.incred.janus.repositories.TransactionLogRepository;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Date;

@Singleton
public class TransactionUtil {

    @Inject
    TransactionLogRepository transactionLogRepository;
    private final ObjectMapper mapper = new ObjectMapper();

    private static final String STARTED = "STARTED";
    public static final String UPLOAD_COMPLETED = "UPLOAD_COMPLETED";
    public static final String COMPLETED = "COMPLETED";

    public TransactionLog initiateTransaction(Request request, String vendor) {
        return transactionLogRepository.save(TransactionLog.builder().applicationId(request.getApplicationId())
                .transactionId(request.getTransactionId())
                .platform(request.getPlatform())
                .sender(request.getSender())
                .status("ACCEPTED").vendor(vendor)
                .createdAt(new Date()).updatedAt(new Date())
                .build());
    }

    public TransactionLog logTransaction(Request request, Response response, String vendor, String category, String status, Long retry) throws JsonProcessingException {
        return transactionLogRepository.save(TransactionLog.builder().applicationId(request.getApplicationId())
                .transactionId(request.getTransactionId())
                .platform(request.getPlatform())
                .sender(request.getSender())
                .category(category).retryCount(retry)
                .input(mapper.writeValueAsString(request))
                .output(mapper.writeValueAsString(response))
                .status(status).vendor(vendor).createdAt(new Date())
                .updatedAt(new Date())
                .build());
    }

    public TransactionLog startCibilTxn(CIBILRequest request) throws JsonProcessingException {
        return transactionLogRepository.save(TransactionLog.builder()
                .applicationId(request.getApplicationId())
                .transactionId(request.getTransactionId())
                .category("BUREAU")
                .platform(request.getPlatform())
                .sender(request.getSender())
                .vendor("TRANS_UNION")
                .input(mapper.writeValueAsString(request))
                .createdAt(new Date()).updatedAt(new Date())
                .status(STARTED)
                .build());
    }

    public TransactionLog startHunterTxn(HunterRequest request) throws JsonProcessingException {
        return transactionLogRepository.save(TransactionLog.builder()
                .applicationId(request.getApplicationId())
                .transactionId(request.getTransactionId())
                .category("BUREAU")
                .platform(request.getPlatform())
                .sender(request.getSender())
                .vendor(HunterUtil.VENDOR_HUNTER)
                .input(mapper.writeValueAsString(request))
                .createdAt(new Date()).updatedAt(new Date())
                .status(STARTED)
                .build());
    }

    public TransactionLog startPerfiosTxn(PerfiosRequest request) throws JsonProcessingException {
        return transactionLogRepository.save(TransactionLog.builder()
                .applicationId(request.getApplicationId())
                .customerId(request.getProfile().getCustomerId())
                .transactionId(request.getTransactionId())
                .category(PerfiosUtil.BANK_TRANSCRIPTION)
                .platform(request.getPlatform())
                .sender(request.getSender())
                .vendor(PerfiosUtil.VENDOR_PERFIOS)
                .input(mapper.writeValueAsString(request))
                .createdAt(new Date()).updatedAt(new Date())
                .status(STARTED)
                .build());
    }

    public void updateTransaction(Response response, TransactionLog log) throws JsonProcessingException {
        response.setTransactionId(log.getTransactionId());
        if (response.getSuccess()) {
            log.setStatus("COMPLETED");
        } else {
            log.setStatus("FAILED");
        }
        log.setUpdatedAt(new Date());
        log.setOutput(mapper.writeValueAsString(response));
        transactionLogRepository.update(log.getId(), log);

    }

    public void updatePerfiosTxnStatus(TransactionLog log, String status) {
        log.setStatus(status);
        log.setUpdatedAt(new Date());
        transactionLogRepository.update(log.getId(), log);
    }
}

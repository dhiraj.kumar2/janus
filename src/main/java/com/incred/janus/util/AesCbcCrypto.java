package com.incred.janus.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.Random;

public class AesCbcCrypto {
    private static String CIPHER_NAME = "AES/CBC/PKCS5PADDING";
    private static int CIPHER_KEY_LEN = 16; // 128 bit
    private static String UTF8 = "UTF-8";// s

    /**
     * Encrypt data using AES Cipher (CBC) with 128 bit key * * @param key * - key to use should be 16 bytes long (128 bits) * @param iv * - initialization vector * @param data
     * <p>
     * - data to encrypt * @return encryptedData data in base64 encoding with iv attached at end * after a :
     */
    public static String encrypt(String key, String iv, String data) {
        try {
            byte[] sha = generateSha512Hash(key.getBytes(UTF8));
            StringBuilder sb = new StringBuilder();
            // converting byte[] type SHA into Hexadecimal representation string
            for (byte b : sha) {
                sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            String shaKey = sb.substring(0, 16);
            IvParameterSpec ivSpec = new IvParameterSpec(iv.getBytes(UTF8));
            SecretKeySpec secretKey = new SecretKeySpec(fixKey(shaKey).getBytes(UTF8), "AES");
            Cipher cipher = Cipher.getInstance(AesCbcCrypto.CIPHER_NAME);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
            byte[] encryptedData = cipher.doFinal((data.getBytes()));
            String encryptedDataInBase64 = Base64.getEncoder().encodeToString(
                    encryptedData);
            String ivInBase64 = Base64.getEncoder().encodeToString(iv.getBytes(UTF8));
            return encryptedDataInBase64 + ":" + ivInBase64;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private static String fixKey(String key) {
        if (key.length() < AesCbcCrypto.CIPHER_KEY_LEN) {
            int numPad = AesCbcCrypto.CIPHER_KEY_LEN - key.length();
            StringBuilder keyBuilder = new StringBuilder(key);
            for (int i = 0; i < numPad; i++) {
                keyBuilder.append("0"); // 0 pad to len 16 bytes
            }
            key = keyBuilder.toString();
            return key;
        }

        if (key.length() > AesCbcCrypto.CIPHER_KEY_LEN) {
            return key.substring(0, CIPHER_KEY_LEN); // truncate to 16 bytes
        }
        return key;
    }

    /**
     * Decrypt data using AES Cipher (CBC) with 128 bit key
     * * @param key * - key to use should be 16 bytes long (128 bits) * @param data * - encrypted data with iv at the end separate by : *                @return decrypted data string
     */
    public static String decrypt(String key, String data) {
        try {
            byte[] sha = generateSha512Hash(key.getBytes(UTF8));
            StringBuilder sb = new StringBuilder();
            for (byte b : sha) {
                sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            String shaKey = sb.substring(0, 16);
            String[] parts = data.split(":");
            IvParameterSpec iv = new IvParameterSpec(Base64.getDecoder().decode(parts[1]));

            SecretKeySpec secretKey = new SecretKeySpec(shaKey.getBytes(UTF8), "AES");

            Cipher cipher = Cipher.getInstance(AesCbcCrypto.CIPHER_NAME);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
            byte[] decodedEncryptedData = Base64.getDecoder().decode(parts[0]);
            byte[] original = cipher.doFinal(decodedEncryptedData);
            return new String(original);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /*
     * This method return IV random values of 16 bit (Init Vector values) which * must be 16 bit long and random each time encrypting the data. */
    protected static String getIV() {
        String ivCHARS = "1234567890";
        StringBuilder iv = new StringBuilder();
        Random rnd = new Random();
        while (iv.length() < 16) { // length of the random string.
            int index = (int) (rnd.nextFloat() * ivCHARS.length());
            iv.append(ivCHARS.charAt(index));
        }
        return iv.toString();
    }

    /**
     * Generate SHA512 Hashes of given data * * @param data * - data to use should be in bytes type array form
     *
     * @return decrypted SHA512 hashes in form of byte type array
     */
    public static byte[] generateSha512Hash(byte[] data) {
        String algorithm = "SHA-512";
        byte[] hash = null;
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance(algorithm);
            digest.reset();
            hash = digest.digest(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hash;
    }
}
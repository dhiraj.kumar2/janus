package com.incred.janus.util;

import com.incred.janus.models.cibil.core.CIBILRequest;
import com.incred.janus.models.cibil.core.CIBILResponse;
import com.incred.janus.models.cibil.core.Nominee;
import com.incred.janus.models.cibil.request.application.*;
import com.incred.janus.models.cibil.request.applicationData.ApplicationDataType;
import com.incred.janus.models.cibil.response.CreditReport;
import com.incred.janus.models.cibil.response.error.ErrorResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Singleton
public class CIBILUtil {

    public static final String APPLICANTS_XML_PATH = "/DCResponse/ContextData/Field[@key='Applicants']";
    public static final String BUREAU_RESPONSE_XML_PATH = "/Applicants/Applicant/DsCibilBureau/Response/CibilBureauResponse/BureauResponseXml";
    public static final String SECONDARY_BUREAU_RESPONSE_XML_PATH = "/Applicants/Applicant/DsCibilBureau/Response/CibilBureauResponse/SecondaryReportXml";
    public static final String APPLICATION_ID_XML_PATH = "/DCResponse/ContextData/Field[@key='ApplicationId']";
    public static final String DOCUMENT_ID_XML_PATH = "/DCResponse/ResponseInfo/DocumentDetails/DocumentMetaData/DocumentId";

    public String toXML(ApplicantsType applicantsType) {
        return toXML(applicantsType, ApplicantsType.class);
    }

    public String toXML(ApplicationDataType applicationDataType) {
        return toXML(applicationDataType, ApplicationDataType.class);
    }

    private String toXML(Object object, Class<?> clazz) {
        if(object == null) return "";

        String xmlString = "";
        try {
            //Create JAXB Context
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);

            //Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(clazz.cast(object), sw);

            //Verify XML Content
            xmlString = sw.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlString;
    }

    public String getCIBILXMLReq(ApplicantsType applicantsType, ApplicationDataType applicationDataType, String uid, String pwd) {
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n" +
                "        <soapenv:Header />\n" +
                "        <soapenv:Body>\n" +
                "          <tem:ExecuteXMLString>\n" +
                "            <tem:request>\n" +
                "              <![CDATA[ \n" +
                "\n" +
                "      <DCRequest xmlns=\"http://transunion.com/dc/extsvc\">\n" +
                "        <Authentication type=\"OnDemand\">\n" +
                "            <UserId>" + uid + "</UserId>\n" +
                "            <Password>" + pwd + "</Password>\n" +
                "         </Authentication>\n" +
                "         <RequestInfo>\n" +
                "              <SolutionSetId>145</SolutionSetId>\n" +
                "              <ExecuteLatestVersion>true</ExecuteLatestVersion>\n" +
                "              <ExecutionMode>NewWithContext</ExecutionMode>\n" +
                "        </RequestInfo>\n" +
                "        <Fields>\n" +
                "          <Field key=\"Applicants\">"+
                                toXML(applicantsType).replaceAll("<", "&lt;").replaceAll(">", "&gt;") +
                "          </Field>\n" +
                "          <Field key=\"ApplicationData\">"+
                                toXML(applicationDataType).replaceAll("<", "&lt;").replaceAll(">", "&gt;") +
                "          </Field>\n"+
                "          <Field key=\"FinalTraceLevel\">2</Field>\n" +
                "          </Fields>\n" +
                "        </DCRequest>\n" +
                "]]>\n" +
                "        </tem:request>\n" +
                "      </tem:ExecuteXMLString>\n" +
                "    </soapenv:Body>\n" +
                "  </soapenv:Envelope>";
    }

    String getUsefulCIBILResponse(String response) throws SOAPException, IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        SOAPMessage message = MessageFactory.newInstance()
                                            .createMessage(null, new ByteArrayInputStream(response.getBytes()));
        NodeList nl = message.getSOAPBody().getChildNodes();
        String DCResponse = nl.item(0).getChildNodes().item(0).getChildNodes().item(0).getTextContent();
        String applicantResponse = getXMLText(DCResponse, APPLICANTS_XML_PATH);
        return getXMLText(applicantResponse, BUREAU_RESPONSE_XML_PATH);
    }

    String getUsefulCIBILResponseSecondary(String response) throws SOAPException, IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        SOAPMessage message = MessageFactory.newInstance()
                                            .createMessage(null, new ByteArrayInputStream(response.getBytes()));
        NodeList nl = message.getSOAPBody().getChildNodes();
        String DCResponse = nl.item(0).getChildNodes().item(0).getChildNodes().item(0).getTextContent();
        String applicantResponse = getXMLText(DCResponse, APPLICANTS_XML_PATH);
        return getXMLText(applicantResponse, SECONDARY_BUREAU_RESPONSE_XML_PATH);
    }

    public String getAppId(String response) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException, SOAPException {
        SOAPMessage message = MessageFactory.newInstance()
                                            .createMessage(null, new ByteArrayInputStream(response.getBytes()));
        NodeList nl = message.getSOAPBody().getChildNodes();
        String DCResponse = nl.item(0).getChildNodes().item(0).getChildNodes().item(0).getTextContent();
        return getXMLText(DCResponse, APPLICATION_ID_XML_PATH);
    }

    public String getDocId(String response) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException, SOAPException {
        SOAPMessage message = MessageFactory.newInstance()
                                            .createMessage(null, new ByteArrayInputStream(response.getBytes()));
        NodeList nl = message.getSOAPBody().getChildNodes();
        String DCResponse = nl.item(0).getChildNodes().item(0).getChildNodes().item(0).getTextContent();
        String nonCheckSUMResponse = DCResponse;
        if (DCResponse.contains("<CheckSum>")) {
            nonCheckSUMResponse = DCResponse.split("<CheckSum>")[0] + DCResponse.split("</CheckSum>")[1];
        }
        return getXMLText(nonCheckSUMResponse, DOCUMENT_ID_XML_PATH);
    }

    public String getHTMLBuffer(String response) throws Exception {
        SOAPMessage message = MessageFactory.newInstance()
                                            .createMessage(null, new ByteArrayInputStream(response.getBytes()));
        return message.getSOAPBody().getTextContent().replaceAll("<\\?.*", "");
    }

    public boolean isSuccess(String response) throws SOAPException, IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        String bureauResponse = getUsefulCIBILResponse(response);
        return getXMLText(bureauResponse, "/Response") == null;
    }

    public CIBILResponse toSuccessPOJO(String response) throws SOAPException, IOException, XPathExpressionException, SAXException, ParserConfigurationException, JAXBException {
        String bureauResponse = getUsefulCIBILResponse(response);
        Unmarshaller unmarshaller = JAXBContext.newInstance(CreditReport.class).createUnmarshaller();
        CreditReport creditReport = (CreditReport) unmarshaller.unmarshal(new StringReader(bureauResponse));
        CreditReport secondaryCreditReport = getSecondaryCreditReport(response);
        CIBILResponse cibilResponse = new CIBILResponse();
        cibilResponse.setCreditReport(creditReport);
        if (secondaryCreditReport != null) {
            cibilResponse.setSecondaryCreditReport(secondaryCreditReport);
        }
        cibilResponse.setCibilAppId(getAppId(response));
        cibilResponse.setSuccess(true);
        return cibilResponse;
    }

    private CreditReport getSecondaryCreditReport(String response) throws SOAPException, IOException, XPathExpressionException, SAXException, ParserConfigurationException, JAXBException {
        try {
            String secondaryBureauResponse = getUsefulCIBILResponseSecondary(response);
            if (secondaryBureauResponse.contains("lt;")) {
                secondaryBureauResponse = getXMLText(secondaryBureauResponse, "/Root");
            }
            if (secondaryBureauResponse.contains("Root")) {
                secondaryBureauResponse = secondaryBureauResponse.replace("<Root>", "").replace("</Root>", "");
            }
            Unmarshaller unmarshaller2 = JAXBContext.newInstance(CreditReport.class).createUnmarshaller();
            return (CreditReport) unmarshaller2.unmarshal(new StringReader(secondaryBureauResponse));
        } catch (Exception e) {
            return null;
        }
    }

    public ErrorResponse toErrorPOJO(String response) throws SOAPException, IOException, XPathExpressionException, SAXException, ParserConfigurationException, JAXBException {
        String bureauResponse = getUsefulCIBILResponse(response);
        Unmarshaller unmarshaller = JAXBContext.newInstance(ErrorResponse.class).createUnmarshaller();
        return (ErrorResponse) unmarshaller.unmarshal(new StringReader(bureauResponse));
    }

    String getXMLText(String body, String path) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        ByteArrayInputStream input =  new ByteArrayInputStream(
                body.getBytes(StandardCharsets.UTF_8));
        Document doc = builder.parse(input);
        XPath xPath =  XPathFactory.newInstance().newXPath();
        NodeList nodeList = (NodeList) xPath.compile(path).evaluate(
                doc, XPathConstants.NODESET);

        if(nodeList.getLength() == 0) return null;
//        for (int i=0; i < nodeList.item(0).getChildNodes().getLength(); i++) System.out.println(nodeList.item(0).getChildNodes().item(i).getNodeName());
        return nodeList.item(0).getTextContent();
    }

    public ApplicantsType toXMLApplicantTypes(CIBILRequest cibilRequest){
        ApplicantsType applicants = new ApplicantsType();
        addApplicantBasicInfo(applicants, cibilRequest);
        addApplicantIdentifiers(applicants, cibilRequest);
        addApplicantTelephone(applicants, cibilRequest);
        addApplicantAddress(applicants, cibilRequest);
        addApplicantAccounts(applicants, cibilRequest);
        addApplicantNominee(applicants, cibilRequest);
        return applicants;
    }

    private void addApplicantNominee(ApplicantsType applicants, CIBILRequest cibilRequest) {
        ApplicantType applicant = applicants.getApplicant();
        Nominee nominee = cibilRequest.getApplicant().getNominee();
        if (nominee != null) {
            applicant.setNomineeRelation(nominee.getNomineeRelation());
            applicant.setNomineeName(nominee.getNomineeName());

            applicant.setMemberRelationType1(nominee.getMemberRelationType1());
            applicant.setMemberRelationName1(nominee.getMemberRelationName1());
            applicant.setMemberRelationType2(nominee.getMemberRelationType2());
            applicant.setMemberRelationName2(nominee.getMemberRelationName2());
            applicant.setMemberRelationType3(nominee.getMemberRelationType3());
            applicant.setMemberRelationName3(nominee.getMemberRelationName3());

            applicant.setKeyPersonRelation(nominee.getKeyPersonRelation());
            applicant.setKeyPersonName(nominee.getKeyPersonName());

            applicant.setMemberOtherId1(nominee.getMemberOtherId1());
            applicant.setMemberOtherId1Type(nominee.getMemberOtherId1Type());
            applicant.setMemberOtherId2(nominee.getMemberOtherId2());
            applicant.setMemberOtherId2Type(nominee.getMemberOtherId2Type());
            applicant.setMemberOtherId3(nominee.getMemberOtherId3());
            applicant.setMemberOtherId3Type(nominee.getMemberOtherId3Type());
        }
    }

    private void addApplicantAccounts(ApplicantsType applicants, CIBILRequest cibilRequest) {
        applicants.getApplicant().setAccounts(new AccountsType());
        List<AccountType> account = applicants.getApplicant().getAccounts().getAccount();
        if (cibilRequest.getApplicant().getAccounts() != null) {
            account.addAll(cibilRequest.getApplicant().getAccounts());
        }
    }

    private void addApplicantAddress(ApplicantsType applicants, CIBILRequest cibilRequest) {
        applicants.getApplicant().setAddresses(new AddressesType());
        List<AddressType> address = applicants.getApplicant().getAddresses().getAddress();
        address.addAll(cibilRequest.getApplicant().getAddresses());
    }

    private void addApplicantTelephone(ApplicantsType applicants, CIBILRequest cibilRequest) {
        applicants.getApplicant().setTelephones(new TelephonesType());
        List<TelephoneType> address = applicants.getApplicant().getTelephones().getTelephone();
        address.addAll(cibilRequest.getApplicant().getTelephones());
    }

    void addApplicantBasicInfo(ApplicantsType applicants, CIBILRequest cibilRequest) {
        applicants.setApplicant(new ApplicantType());
        ApplicantType applicant = applicants.getApplicant();
        applicant.setApplicantType(cibilRequest.getApplicant().getApplicantType());
        applicant.setApplicantFirstName(cibilRequest.getApplicant().getApplicantFirstName());
        applicant.setApplicantMiddleName(cibilRequest.getApplicant().getApplicantMiddleName());
        applicant.setApplicantLastName(cibilRequest.getApplicant().getApplicantLastName());
        applicant.setDateOfBirth(cibilRequest.getApplicant().getDateOfBirth());
        applicant.setGender(cibilRequest.getApplicant().getGender());
        applicant.setEmailAddress(cibilRequest.getApplicant().getEmailAddress());
        applicant.setCompanyName(cibilRequest.getApplicant().getCompanyName());
    }

    void addApplicantIdentifiers(ApplicantsType applicants, CIBILRequest cibilRequest) {
        applicants.getApplicant().setIdentifiers(new IdentifiersType());
        if(cibilRequest.getApplicant().getIdentifier()!= null) {
            List<IdentifierType> identifier = applicants.getApplicant().getIdentifiers().getIdentifier();
            identifier.addAll(cibilRequest.getApplicant().getIdentifier());
        }
    }

    public String getDocXMLReq(String appId, String uid, String pwd) {
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n" +
                "        <soapenv:Header />\n" +
                "        <soapenv:Body>\n" +
                "          <tem:RetrieveDocumentMetaDataXMLString>\n" +
                "            <tem:request>\n" +
                "              <![CDATA[ \n" +
                "\t\t<DCRequest xmlns=\"http://transunion.com/dc/extsvc\">\n" +
                "  <Authentication type=\"Token\">\n" +
                "    <UserId>" + uid + "</UserId>\n" +
                "    <Password>" + pwd + "</Password>\n" +
                "  </Authentication>\n" +
                "  <RetrieveDocumentMetaData>\n" +
                "    <ApplicationId>" + appId + "</ApplicationId>\n" +
                "  </RetrieveDocumentMetaData>\n" +
                "</DCRequest>        \n" +
                "   ]]>\n" +
                "        </tem:request>\n" +
                "      </tem:RetrieveDocumentMetaDataXMLString>\n" +
                "    </soapenv:Body>\n" +
                "  </soapenv:Envelope>";
    }

    public String getDocBufferXMLReq(String appId, String docId, String uid, String pwd) {
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n" +
                "        <soapenv:Header />\n" +
                "        <soapenv:Body>\n" +
                "          <tem:DownloadDocument>\n" +
                "            <tem:request>\n" +
                "              <![CDATA[ \n" +
                "\t\t<DCRequest xmlns=\"http://transunion.com/dc/extsvc\">\n" +
                "  <Authentication type=\"Token\">\n" +
                "    <UserId>" + uid + "</UserId>\n" +
                "    <Password>" + pwd + "</Password>\n" +
                "  </Authentication>\n" +
                "  <DownloadDocument>\n" +
                "    <ApplicationId>" + appId + "</ApplicationId>\n" +
                "    <DocumentId>" + docId + "</DocumentId>\n" +
                "  </DownloadDocument>\n" +
                "</DCRequest>        \n" +
                "   ]]>\n" +
                "        </tem:request>\n" +
                "      </tem:DownloadDocument>\n" +
                "    </soapenv:Body>\n" +
                "  </soapenv:Envelope>";
    }
}

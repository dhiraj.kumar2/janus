package com.incred.janus.util;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.multipart.CompletedFileUpload;
import io.micronaut.http.multipart.FileUpload;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.inject.Singleton;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
@Singleton
public class S3Util {

    @Value("${aws.accessKey}")
    private String accessKey;
    @Value("${aws.secretAccessKey}")
    private String secretAccessKey;
    @Value("${aws.region}")
    private String region;

    private AmazonS3 s3client;

    public static final String PATH = "test/";

    private synchronized void init() {
        if (s3client == null) {
            AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretAccessKey);
            s3client = AmazonS3ClientBuilder
                    .standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .withRegion(Regions.fromName(region))
                    .build();
        }
    }

    public boolean exists(String bucketName, String key) {
        init();
        return s3client.doesObjectExist(bucketName, key);
    }

    public File get(String bucketName, String key) throws Exception {
        init();
        S3Object s3Object = s3client.getObject(bucketName, key);
        InputStream inputStream = s3Object.getObjectContent();
        byte[] buffer = new byte[1024];
        Path tempFilePath = Files.createTempFile("temp", ".pdf");

        OutputStream outputStream = new FileOutputStream(tempFilePath.toFile());
        int count;
        while ((count = inputStream.read(buffer)) != -1) {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            outputStream.write(buffer, 0, count);
        }

        outputStream.close();
        inputStream.close();
        return tempFilePath.toFile();
    }

    public String put(CompletedFileUpload fileUpload, String bucketName) {
        init();
        try {
            String key = getKey(PATH + fileUpload.getFilename());
            s3client.putObject(bucketName, key,
                    fileUpload.getInputStream(), createObjectMetadata(fileUpload));
            return key;
        } catch (Exception e) {
            log.error("error while uploading file to s3 {}", ExceptionUtils.getStackTrace(e));
            return "";
        }
    }

    private ObjectMetadata createObjectMetadata(FileUpload fileUpload) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        fileUpload.getContentType().ifPresent(contentType -> objectMetadata.setContentType(contentType.getName()));
        if (fileUpload.getSize() != 0) {
            objectMetadata.setContentLength(fileUpload.getSize());
        }
        return objectMetadata;
    }

    private String getKey(String path) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(path.getBytes());

        byte byteData[] = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++)
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        return sb.toString();
    }
}

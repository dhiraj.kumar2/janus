package com.incred.janus.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incred.janus.models.core.RestResponse;
import io.micronaut.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.inject.Singleton;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.CompletableFuture;


@Singleton
public class RestUtil {

    private static final Logger log = LoggerFactory.getLogger(RestUtil.class);
    private final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
    private Duration duration = Duration.ofMinutes(1);
    private ObjectMapper mapper = new ObjectMapper();

    private static HttpResponse<String> logReceipt(HttpResponse<String> res) {
        log.info("Received response");
        return res;
    }

    public Mono<RestResponse> post(String url, Object body, Map<String, String> headers, String contentType) throws Exception {
        log.info("Making post call with url : " + url);
        HttpRequest.Builder requestBuilder = HttpRequest.newBuilder().uri(URI.create(url));
        if (body instanceof String) {
            requestBuilder.POST(BodyPublishers.ofString((String) body));
        } else {
            requestBuilder.POST(BodyPublishers.ofString(mapper.writeValueAsString(body)));
        }
        headers.forEach(requestBuilder::setHeader);
        requestBuilder.setHeader("Content-Type", contentType);
        HttpRequest request = requestBuilder.timeout(duration).build();
        try {

            CompletableFuture<RestResponse> restResponseCompletableFuture = httpClient
                    .sendAsync(request, HttpResponse.BodyHandlers.ofString()).thenApply(RestUtil::logReceipt)
                    .thenApply(response -> new RestResponse(response.body(), response.statusCode(),
                                                            response.headers()));
            return Mono.just(restResponseCompletableFuture.get()).log().subscribeOn(Schedulers.elastic());
        } catch (Exception e) {
            log.info("Exception occurred while calling url : " + url);
            return Mono.just(new RestResponse(null, HttpStatus.SERVICE_UNAVAILABLE.getCode(), null));
        }
    }

    public Mono<RestResponse> get(String url, Map<String, String> headers) throws Exception {
        HttpRequest.Builder requestBuilder = HttpRequest.newBuilder().uri(URI.create(url)).GET();
        headers.forEach(requestBuilder::setHeader);
        HttpRequest request = requestBuilder.timeout(duration).build();
        return Mono.fromFuture(httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                                         .thenApply(response -> new RestResponse(response.body(), response.statusCode(),
                                                                                 response.headers())));
    }
}

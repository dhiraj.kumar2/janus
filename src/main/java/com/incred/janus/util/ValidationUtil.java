package com.incred.janus.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incred.janus.models.authbridge.client.ValidationRequest;
import com.incred.janus.models.authbridge.client.ValidationResponse;
import com.incred.janus.models.authbridge.vendor.AuthBridgeVendorResponse;
import com.incred.janus.models.authbridge.vendor.AuthBridgeVendorResponseParam;
import com.incred.janus.models.core.RestResponse;
import com.incred.janus.models.karza.client.PanData;
import com.incred.janus.models.karza.vendor.KarzaResponse;
import io.micronaut.context.annotation.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

@Singleton
public class ValidationUtil {
    private static final Logger log = LoggerFactory.getLogger(ValidationUtil.class);
    private final static ObjectMapper objectMapper = new ObjectMapper();
    public static final String NOT_PRESENT = "Not present in Income Tax Department (ITD) database/Invalid PAN";
    private AtomicInteger errorCount = new AtomicInteger(0);
    private Long errorLastSet = 0l;
    @Value("${validation.primary}")
    private String primary;
    @Value("${validation.secondary}")
    private String secondary;
    @Value("${error.interval}")
    public Long errorInterval;
    @Value("${error.errorCountConfig}")
    public Long errorCountConfig;


    public ValidationResponse parseAuthBridgeResponse(RestResponse restResponse, String password) throws IOException {
        log.info("Parsing AuthBridge response ");
        if (restResponse == null || restResponse.getBody() == null) {
            countErrors();
            return new ValidationResponse(false, "Error calling service", null);
        } else if (restResponse.getCode() / 200 == 1) {
            try {
                AuthBridgeVendorResponse authBridgeVendorResponse = objectMapper
                        .readValue(restResponse.getBody(), AuthBridgeVendorResponse.class);
                if (authBridgeVendorResponse == null) {
                    countErrors();
                    return new ValidationResponse(false, "No response from vendor", null);
                } else if (authBridgeVendorResponse.getResponseData() != null) {
                    String decrypt = AesCbcCrypto.decrypt(password, authBridgeVendorResponse.getResponseData());
                    AuthBridgeVendorResponseParam authBridgeVendorResponseParam = objectMapper
                            .readValue(decrypt, AuthBridgeVendorResponseParam.class);
                    boolean invalidPAN = NOT_PRESENT.equals(authBridgeVendorResponseParam.getMessage());
                    boolean success = authBridgeVendorResponseParam.getStatus().equals("1");
                    return new ValidationResponse(success || invalidPAN,
                            success ? null : (String) authBridgeVendorResponseParam.getMessage(),
                            success ? authBridgeVendorResponseParam.getMessage() : null);
                } else {
                    countErrors();
                    return new ValidationResponse(false, authBridgeVendorResponse.getMessage(), null);
                }
            } catch (Exception e) {
                countErrors();
                return new ValidationResponse(false, "No response from vendor", null);
            }
        } else {
            AuthBridgeVendorResponse authBridgeVendorResponse = objectMapper
                    .readValue(restResponse.getBody(), AuthBridgeVendorResponse.class);
            return new ValidationResponse(false, authBridgeVendorResponse.getMessage(), null);
        }
    }

    public ValidationResponse parseKarzaResponse(RestResponse restResponse, ValidationRequest validationRequest) throws IOException {
        log.info("Parsing Karza response ");
        if (restResponse == null || restResponse.getBody() == null) {
            countErrors();
            return new ValidationResponse(false, "Error calling service", null);
        } else if (restResponse.getCode() / 200 == 1) {
            try {
                KarzaResponse response = objectMapper.readValue(restResponse.getBody(), KarzaResponse.class);
                if (response.getStatusCode().equals("101")) {
                    return new ValidationResponse(true, null,
                            new PanData(validationRequest.getDocNumber(),
                                    response.getResult().getName(), "20-12-2019", "Active",
                                    "Active and Valid"));
                } else {
                    return new ValidationResponse(true, NOT_PRESENT, null);
                }
            } catch (Exception e) {
                countErrors();
                return new ValidationResponse(false, "No response from vendor", null);
            }
        }
        if (restResponse.getBody() != null) {
            countErrors();
            KarzaResponse response = objectMapper.readValue(restResponse.getBody(), KarzaResponse.class);
            return new ValidationResponse(false, "Error obtaining PAN data : " + response.getError(), null);

        }
        countErrors();
        return new ValidationResponse(false, "Error obtaining PAN data", null);
    }

    public String getVendor(String type) {
        switch (type) {
            case "primary":
                return primary;
            case "secondary":
                return secondary;
        }
        return null;
    }

    public void swapProviders() {
        String temp = primary;
        primary = secondary;
        secondary = temp;
    }

    public void countErrors() {
        checkAndResetErrorCount(errorInterval);
        errorCount.getAndIncrement();
        errorLastSet = System.currentTimeMillis();
        if (errorCount.get() > errorCountConfig) {
            swapProviders();
            resetCounter();
        }
    }

    public void checkAndResetErrorCount(Long errorInterval) {
        if ((System.currentTimeMillis() - errorLastSet) > (errorInterval * 1000)) {
            resetCounter();
        }
    }

    public void resetCounter() {
        errorCount = new AtomicInteger(0);
        errorLastSet = System.currentTimeMillis();
    }
}

package com.incred.janus.util;

import com.incred.janus.models.hunter.vendor.request.SUBMISSION;
import com.incred.janus.models.hunter.vendor.response.MatchResponse;
import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.Date;
import java.util.GregorianCalendar;

@Singleton
public class HunterUtil {

    public static final String VENDOR_HUNTER = "HUNTER";

    public XMLGregorianCalendar dateToXMLGregorianCalenderDate(Date date) {
        XMLGregorianCalendar xmlDate = null;
        GregorianCalendar gc = new GregorianCalendar();
        if (date != null) {
            gc.setTime(date);
            try {
                xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            } catch (Exception e) {
                e.printStackTrace();
            }//TODO : remove
        } else {
            gc.setTime(new Date());
            try {
                xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return xmlDate;
    }

    public int genderToCode(String gender) {
        switch (gender.toUpperCase()) {
            case "OTHER":
                return 1;
            case "MALE":
                return 2;
            case "FEMALE":
                return 3;
            default:
                return 4;
        }
    }

    public int maritalStatusToCode(String maritalStatus) {
        switch (maritalStatus.toUpperCase()) {
            case "MARRIED":
                return 0;
            case "SINGLE":
                return 1;
            case "DIVORCED":
                return 2;
            case "SEPARATED":
                return 3;
            case "WIDOWED":
                return 4;
            case "LIVING WITH PARTNER":
                return 5;
            case "CIVIL PARTNERSHIP":
                return 6;
            default:
                return 99;
        }
    }

    public String toXML(SUBMISSION submission) {
        String res = "";
        try {
            //Create JAXB Context
            JAXBContext jaxbContext = JAXBContext.newInstance(SUBMISSION.class);

            //Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(submission, sw);

            //Verify XML Content
            res = sw.toString().replace("</SUBMISSION>", "").replace("<SUBMISSION>", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public MatchResponse toPOJO(String xml) {
        try {
            xml = xml.replaceAll("&lt;", "<");
            xml = xml.replaceAll("&gt;", ">");
            xml = xml.replace("xmlns=\"http://www.mclsoftware.co.uk/HunterII/WebServices\"", "");
            SOAPMessage message = MessageFactory.newInstance()
                                                .createMessage(null, new ByteArrayInputStream(xml.getBytes()));
            Unmarshaller unmarshaller = JAXBContext.newInstance(MatchResponse.class).createUnmarshaller();
            return (MatchResponse) unmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @NotNull
    public String getHunterXML(String body, String username, String password, int schemeSetID, int hunterCustomerId) {
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.mclsoftware.co.uk/HunterII/WebServices\">\n" +
                "    <soapenv:Header/>\n" +
                "    <soapenv:Body>\n" +
                "       <web:Match>\n" +
                "          <web:controlXml>\n" +
                "          <![CDATA[ \n" +
                "             <ControlBlock xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"urn:mclsoftware.co.uk:hunterII\"> \n" +
                "             <Customer>\n" +
                "                 <CustomerID>" + hunterCustomerId + "</CustomerID>\n" +
                "                 <CustomerName>INCRED</CustomerName>\n" +
                "             </Customer>\n" +
                "             <Loading>\n" +
                "                 <SubmissionLoad>1</SubmissionLoad>\n" +
                "                 <SuppressVersion flag=\"0\" />\n" +
                "             </Loading>\n" +
                "             <Matching>\n" +
                "                 <MatchSchemeSet>\n" +
                "                     <SchemeSetID>21</SchemeSetID>\n" +
                "                     <SchemeSetID>" + schemeSetID + "</SchemeSetID>\n" +
                "                 </MatchSchemeSet>\n" +
                "                 <PersistMatches>1</PersistMatches>\n" +
                "                 <WorklistInsert>1</WorklistInsert>\n" +
                "             </Matching>\n" +
                "             <Results>\n" +
                "                 <RsltCode>1</RsltCode>\n" +
                "             </Results> \n" +
                "             </ControlBlock> \n" +
                "             ]]>\n" +
                "             </web:controlXml>\n" +
                "          <web:batchXml>  \n" +
                "          <![CDATA[ <BATCH xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"urn:mclsoftware.co.uk:hunterII\"> \n" +
                "                <HEADER>\n" +
                "                    <COUNT>1</COUNT>\n" +
                "                    <ORIGINATOR>INCRED</ORIGINATOR>\n" +
                "                </HEADER>\n" +
                "                <SUBMISSIONS>\n" +
                "                    <SUBMISSION NotificationRqd=\"1\">" + body
                .replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "") + "</SUBMISSION>\n" +
                "                </SUBMISSIONS>\n" +
                "                </BATCH>\n" +
                "                ]]>\n" +
                "                </web:batchXml>\n" +
                "                        <web:username>" + username + "</web:username>\n" +
                "                        <web:password>" + password + "</web:password>\n" +
                "                    </web:Match>\n" +
                "                </soapenv:Body>\n" +
                "        </soapenv:Envelope>";
    }
}
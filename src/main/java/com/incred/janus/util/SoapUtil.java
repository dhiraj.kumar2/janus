package com.incred.janus.util;

import com.incred.janus.models.perfios.response.SOAPResponse;
import io.micronaut.http.HttpStatus;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.inject.Singleton;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import java.io.*;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Map;

@Singleton
@Slf4j
public class SoapUtil {

    public String post(String url, Map<String, String> headers, String body) throws Exception {
        SSLContext sc = prepareSSLContext();
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setSSLSocketFactory(sc.getSocketFactory());
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "text/xml");
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            con.setRequestProperty(entry.getKey(), entry.getValue());
        }

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(body);
        wr.flush();
        wr.close();

        BufferedReader in = null;
        String inputLine;
        StringBuilder response = new StringBuilder();

        try {
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (Exception e) {
            log.error("error in sending post request {} {}", url, ExceptionUtils.getStackTrace(e));
        }

        int responseCode = -1;
        try {
            responseCode = con.getResponseCode();
        } catch (IOException e) {
            log.error("error in sending post request {} {}", url, ExceptionUtils.getStackTrace(e));
        }

        if (in == null && responseCode != HttpStatus.OK.getCode() && con.getErrorStream() != null) {
            in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }

        in.close();

        return response.toString();
    }

    public SOAPResponse send(String url, Map<String, String> headers, String body) {
        try {
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("POST");

            for (Map.Entry<String, String> entry : headers.entrySet()) {
                con.setRequestProperty(entry.getKey(), entry.getValue());
            }

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(body);
            wr.flush();
            wr.close();

            BufferedReader in = null;
            String inputLine;
            StringBuilder response = new StringBuilder();

            try {
                in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            } catch (IOException e) {
                log.error("error in sending post request {} {}", url, ExceptionUtils.getStackTrace(e));
            }

            int responseCode = -1;
            try {
                responseCode = con.getResponseCode();
            } catch (IOException e) {
                log.error("error in sending post request {} {}", url, ExceptionUtils.getStackTrace(e));
            }

            if (in == null && responseCode != HttpStatus.OK.getCode() && con.getErrorStream() != null) {
                in = new BufferedReader(new InputStreamReader(con.getErrorStream()));

                response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }
            in.close();

            return new SOAPResponse(responseCode, response.toString());
        } catch (IOException e) {
            log.error("error in sending soap request {}", ExceptionUtils.getStackTrace(e));
            return new SOAPResponse(HttpStatus.INTERNAL_SERVER_ERROR.getCode(), "Internal Server error");
        }
    }

    private SSLContext prepareSSLContext() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        // openssl pkcs12 -export -in cert.pem -inkey key.pem -out pkcs.p12 -name hunter
        KeyStore ks = KeyStore.getInstance("PKCS12");
        InputStream fis = getClass().getClassLoader().getResourceAsStream("hunter/pkcs.p12");
        //FileInputStream fis = new FileInputStream("/path/to/file.p12");
        ks.load(fis, "hunter".toCharArray());
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, "hunter".toCharArray());
        SSLContext sc = SSLContext.getInstance("TLS");
        sc.init(kmf.getKeyManagers(), null, null);
        return sc;
    }

}

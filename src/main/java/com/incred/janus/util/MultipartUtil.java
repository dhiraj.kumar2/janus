package com.incred.janus.util;

import com.incred.janus.services.PerfiosService;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class MultipartUtil {
    public static Response okHttp(String perfiosTxnId, File file, String vendor, String url, String password) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        RequestBody body;

        String mimeType = Files.probeContentType(file.toPath());

        if (StringUtils.isNotBlank(password)) {
            body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("file", file.getName(),
                            RequestBody.create(MediaType.parse(mimeType), file))
                    .addFormDataPart("perfiosTransactionId", perfiosTxnId)
                    .addFormDataPart("vendorId", vendor)
                    .addFormDataPart("password", password)
                    .build();
        } else {
            body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("file", file.getName(),
                            RequestBody.create(MediaType.parse(mimeType), file))
                    .addFormDataPart("perfiosTransactionId", perfiosTxnId)
                    .addFormDataPart("vendorId", vendor)
                    .build();
        }
        Request request = new Request.Builder()
                .url(url)
                .method("POST", body)
                .addHeader(PerfiosService.CONTENT_TYPE, PerfiosService.FORM_URLENCODED)
                .build();
        return client.newCall(request).execute();
    }
}

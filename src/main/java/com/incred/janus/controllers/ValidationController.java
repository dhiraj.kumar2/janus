package com.incred.janus.controllers;

import com.incred.janus.models.authbridge.client.ValidationRequest;
import com.incred.janus.models.authbridge.client.ValidationResponse;
import com.incred.janus.services.ValidationService;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import reactor.core.publisher.Mono;

import javax.inject.Inject;
import javax.validation.Valid;

@Controller("/validate")
public class ValidationController {

    @Inject
    ValidationService validationService;

    @Post
    @Version("1")
    @Consumes("application/json")
    @Produces("application/json")
    //@TransactionLog
    public Mono<ValidationResponse> validate(@Valid ValidationRequest validationRequest, HttpHeaders httpHeaders) throws Exception {
        return validationService.validate(validationRequest);
    }


}

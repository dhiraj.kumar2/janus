package com.incred.janus.controllers;

import com.incred.janus.models.cibil.core.CIBILReportRequest;
import com.incred.janus.models.cibil.core.CIBILReportResponse;
import com.incred.janus.models.cibil.core.CIBILRequest;
import com.incred.janus.models.cibil.core.CIBILResponse;
import com.incred.janus.services.CIBILService;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.annotation.*;
import reactor.core.publisher.Mono;

import javax.inject.Inject;
import javax.validation.Valid;

@Controller("/cibil")
public class CIBILController {

    @Inject
    CIBILService cibilService;

    @Post
    @Version("1")
    @Consumes("application/json")
    @Produces("application/json")
    public Mono<CIBILResponse> check(@Valid CIBILRequest cibilRequest, HttpHeaders httpHeaders) {
        return Mono.just(cibilService.request(cibilRequest));
    }

    @Get("/{appId}")
    @Version("1")
    @Consumes("application/json")
    @Produces("application/json")
    public Mono<CIBILReportResponse> download(@PathVariable String appId, HttpHeaders httpHeaders) {
        return Mono.just(cibilService.report(new CIBILReportRequest(appId)));
    }
}

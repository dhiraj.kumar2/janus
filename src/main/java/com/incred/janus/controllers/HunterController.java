package com.incred.janus.controllers;

import com.incred.janus.models.hunter.client.HunterRequest;
import com.incred.janus.models.hunter.client.HunterResponse;
import com.incred.janus.services.HunterService;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import reactor.core.publisher.Mono;

import javax.inject.Inject;
import javax.validation.Valid;

@Controller("/hunter")
public class HunterController {

    @Inject
    HunterService hunterService;

    @Post
    @Version("1")
    @Consumes("application/json")
    @Produces("application/json")
    public Mono<HunterResponse> check(@Valid HunterRequest hunterRequest, HttpHeaders httpHeaders) {
        return Mono.just(hunterService.makeHunterRequest(hunterRequest));
    }
}

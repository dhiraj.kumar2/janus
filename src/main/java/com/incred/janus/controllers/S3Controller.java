package com.incred.janus.controllers;

import com.incred.janus.util.S3Util;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.multipart.CompletedFileUpload;

import javax.inject.Inject;

@Controller("/s3")
public class S3Controller {

    @Inject
    S3Util s3Util;

    @Post("/upload")
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public String upload(CompletedFileUpload file, String bucket) {
        return s3Util.put(file, bucket);
    }
}

package com.incred.janus.controllers;

import com.incred.janus.models.core.TransactionLog;
import com.incred.janus.models.perfios.client.PerfiosRequest;
import com.incred.janus.models.perfios.client.PerfiosResponse;
import com.incred.janus.models.perfios.response.RetrieveReport;
import com.incred.janus.models.perfios.response.TxnStatus;
import com.incred.janus.services.PerfiosService;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.annotation.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import reactor.core.publisher.Mono;

import javax.inject.Inject;

@Controller("/perfios")
@Slf4j
public class PerfiosController {

    @Inject
    PerfiosService perfiosService;

    @Post("/start")
    @Version("1")
    @Consumes("application/json")
    @Produces("application/json")
    public Mono<PerfiosResponse> start(PerfiosRequest request, HttpHeaders httpHeaders) {
        try {
            return perfiosService.start(request);
        } catch (Exception e) {
            log.error("error in starting perfios txn {}", ExceptionUtils.getStackTrace(e));
            return Mono.empty();
        }
    }

    @Post("/status")
    @Version("1")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Mono<TxnStatus> checkStatus(String perfiosTxnId, String clientTxnId, HttpHeaders httpHeaders) {
        try {
            return perfiosService.checkStatus(perfiosTxnId, clientTxnId, new TransactionLog());
        } catch (Exception e) {
            log.error("error in checking perfios txn status {}", ExceptionUtils.getStackTrace(e));
            return Mono.empty();
        }
    }

    @Post("/report")
    @Version("1")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Mono<RetrieveReport> getReport(String perfiosTxnId, String clientTxnId, HttpHeaders httpHeaders) {
        try {
            return perfiosService.retrieveReport(perfiosTxnId, clientTxnId, new TransactionLog());
        } catch (Exception e) {
            log.error("error in checking perfios txn status {}", ExceptionUtils.getStackTrace(e));
            return Mono.empty();
        }
    }

    @Get("/test")
    public String test() {
        return "Welcome to Janus";
    }
}

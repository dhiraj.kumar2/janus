package com.incred.janus.controllers;

import com.incred.janus.util.MetricsUtil;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

import javax.inject.Inject;

@Controller("/custom")
public class MetricsController {

    @Inject
    MetricsUtil metricsUtil;

    @Get
    public String scrape() throws Exception {
        return metricsUtil.scrapeMetrics();
    }
}

package com.incred.janus.models.hunter.client;

import lombok.Data;

import java.math.BigInteger;

@Data
public class Reference {
    private String name;
    private BigInteger mobile;
}

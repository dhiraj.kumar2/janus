package com.incred.janus.models.hunter.client;

import lombok.Data;

import java.math.BigInteger;

@Data
public class Address {
    private String address;
    private String city;
    private String state;
    private String country;
    private BigInteger pinCode;
}

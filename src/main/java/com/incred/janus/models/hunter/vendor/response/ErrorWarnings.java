//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0-b170531.0717 
//         See <a href="https://jaxb.java.net/">https://jaxb.java.net/</a> 
//         Any modifications to this file will be lost upon recompilation of the source schema. 
//         Generated on: 2019.12.30 at 02:16:11 PM IST 
//


package com.incred.janus.models.hunter.vendor.response;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected         content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mclsoftware.co.uk/HunterII/WebServices}Errors"/&gt;
 *         &lt;element ref="{http://www.mclsoftware.co.uk/HunterII/WebServices}Warnings"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "errors",
        "warnings"
})
@XmlRootElement(name = "ErrorWarnings")
public class ErrorWarnings {

    @XmlElement(name = "Errors", required = true)
    protected Errors errors;
    @XmlElement(name = "Warnings", required = true)
    protected Warnings warnings;

    /**
     * Gets the value of the errors property.
     *
     * @return possible object is
     * {@link Errors }
     */
    public Errors getErrors() {
        return errors;
    }

    /**
     * Sets the value of the errors property.
     *
     * @param value allowed object is
     *              {@link Errors }
     */
    public void setErrors(Errors value) {
        this.errors = value;
    }

    /**
     * Gets the value of the warnings property.
     *
     * @return possible object is
     * {@link Warnings }
     */
    public Warnings getWarnings() {
        return warnings;
    }

    /**
     * Sets the value of the warnings property.
     *
     * @param value allowed object is
     *              {@link Warnings }
     */
    public void setWarnings(Warnings value) {
        this.warnings = value;
    }

}

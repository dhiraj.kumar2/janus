package com.incred.janus.models.hunter.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JointApplication {
    private String pan;
    private String firstName;
    private BigInteger gender;
    private BigInteger maritalStatus;
    private BigInteger netMonthlySalary;
    private String aadhar;
    private Address presentAddress;
}

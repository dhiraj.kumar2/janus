package com.incred.janus.models.hunter.client;

import lombok.Data;

@Data
public class EmploymentDetails {
    private String companyName;
    private Double netMonthlySalary;
    private Address address;
}

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0-b170531.0717 
//         See <a href="https://jaxb.java.net/">https://jaxb.java.net/</a> 
//         Any modifications to this file will be lost upon recompilation of the source schema. 
//         Generated on: 2019.12.20 at 06:55:28 PM IST 
//


package com.incred.janus.models.hunter.vendor.request;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigInteger;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected         content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}PAN"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}FST_NME"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}DOB"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}AGE"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}GNDR"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}MAR_STT"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}INC"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}NAT_CDE"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}JA_CA"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}JA_ID"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "pan",
        "fstnme",
        "dob",
        "age",
        "gndr",
        "marstt",
        "inc",
        "natcde",
        "jaca",
        "jaid"
})
@XmlRootElement(name = "JA")
public class JA {

    @XmlElement(name = "PAN", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String pan;
    @XmlElement(name = "FST_NME", required = true)
    protected String fstnme;
    @XmlElement(name = "DOB", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NMTOKEN")
    protected String dob;
    @XmlElement(name = "AGE", required = true)
    protected BigInteger age;
    @XmlElement(name = "GNDR", required = true)
    protected BigInteger gndr;
    @XmlElement(name = "MAR_STT", required = true)
    protected BigInteger marstt;
    @XmlElement(name = "INC", required = true)
    protected BigInteger inc;
    @XmlElement(name = "NAT_CDE", required = true)
    protected String natcde;
    @XmlElement(name = "JA_CA", required = true)
    protected JACA jaca;
    @XmlElement(name = "JA_ID", required = true)
    protected JAID jaid;

    public JA(Builder builder) {
        this.pan = builder.pan;
        this.fstnme = builder.fstnme;
        this.dob = builder.dob;
        this.age = builder.age;
        this.gndr = builder.gndr;
        this.marstt = builder.marstt;
        this.inc = builder.inc;
        this.natcde = builder.natcde;
        this.jaca = builder.jaca;
        this.jaid = builder.jaid;

    }

    public JA() {
    }

    /**
     * Gets the value of the pan property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPAN() {
        return pan;
    }

    /**
     * Sets the value of the pan property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPAN(String value) {
        this.pan = value;
    }

    /**
     * Gets the value of the fstnme property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFSTNME() {
        return fstnme;
    }

    /**
     * Sets the value of the fstnme property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFSTNME(String value) {
        this.fstnme = value;
    }

    /**
     * Gets the value of the dob property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDOB() {
        return dob;
    }

    /**
     * Sets the value of the dob property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDOB(String value) {
        this.dob = value;
    }

    /**
     * Gets the value of the age property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getAGE() {
        return age;
    }

    /**
     * Sets the value of the age property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setAGE(BigInteger value) {
        this.age = value;
    }

    /**
     * Gets the value of the gndr property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getGNDR() {
        return gndr;
    }

    /**
     * Sets the value of the gndr property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setGNDR(BigInteger value) {
        this.gndr = value;
    }

    /**
     * Gets the value of the marstt property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getMARSTT() {
        return marstt;
    }

    /**
     * Sets the value of the marstt property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setMARSTT(BigInteger value) {
        this.marstt = value;
    }

    /**
     * Gets the value of the inc property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getINC() {
        return inc;
    }

    /**
     * Sets the value of the inc property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setINC(BigInteger value) {
        this.inc = value;
    }

    /**
     * Gets the value of the natcde property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNATCDE() {
        return natcde;
    }

    /**
     * Sets the value of the natcde property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNATCDE(String value) {
        this.natcde = value;
    }

    /**
     * Gets the value of the jaca property.
     *
     * @return possible object is
     * {@link JACA }
     */
    public JACA getJACA() {
        return jaca;
    }

    /**
     * Sets the value of the jaca property.
     *
     * @param value allowed object is
     *              {@link JACA }
     */
    public void setJACA(JACA value) {
        this.jaca = value;
    }

    /**
     * Gets the value of the jaid property.
     *
     * @return possible object is
     * {@link JAID }
     */
    public JAID getJAID() {
        return jaid;
    }

    /**
     * Sets the value of the jaid property.
     *
     * @param value allowed object is
     *              {@link JAID }
     */
    public void setJAID(JAID value) {
        this.jaid = value;
    }


    public static class Builder {
        private String pan;
        private String fstnme;
        private String dob;
        private BigInteger gndr;
        private BigInteger age;
        private BigInteger marstt;
        private BigInteger inc;
        private String natcde;
        private JACA jaca;
        private JAID jaid;

        public JA.Builder fstnme(String fstnme) {
            this.fstnme = fstnme;
            return this;
        }

        public JA.Builder pan(String pan) {
            this.pan = pan;
            return this;
        }

        public JA.Builder dob(String dob) {
            this.dob = dob;
            return this;
        }

        public JA.Builder gndr(BigInteger gndr) {
            this.gndr = gndr;
            return this;
        }

        public JA.Builder age(BigInteger age) {
            this.age = age;
            return this;
        }

        public JA.Builder marstt(BigInteger marstt) {
            this.marstt = marstt;
            return this;
        }

        public JA.Builder inc(BigInteger inc) {
            this.inc = inc;
            return this;
        }

        public JA.Builder natcde(String natcde) {
            this.natcde = natcde;
            return this;
        }

        public JA.Builder jaca(JACA jaca) {
            this.jaca = jaca;
            return this;
        }

        public JA.Builder jaid(JAID jaid) {
            this.jaid = jaid;
            return this;
        }

        public JA.Builder fromPrototype(JA prototype) {
            fstnme = prototype.fstnme;
            pan = prototype.pan;
            gndr = prototype.gndr;
            dob = prototype.dob;
            age = prototype.age;
            inc = prototype.inc;
            jaid = prototype.jaid;
            marstt = prototype.marstt;
            jaca = prototype.jaca;
            natcde = prototype.natcde;

            return this;
        }

        public JA build() { return new JA(this);
        }
    }

}

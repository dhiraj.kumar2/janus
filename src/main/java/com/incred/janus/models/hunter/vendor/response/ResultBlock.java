//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0-b170531.0717 
//         See <a href="https://jaxb.java.net/">https://jaxb.java.net/</a> 
//         Any modifications to this file will be lost upon recompilation of the source schema. 
//         Generated on: 2019.12.30 at 02:16:11 PM IST 
//


package com.incred.janus.models.hunter.vendor.response;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected         content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mclsoftware.co.uk/HunterII/WebServices}MatchSummary"/&gt;
 *         &lt;element ref="{http://www.mclsoftware.co.uk/HunterII/WebServices}ErrorWarnings"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "matchSummary",
        "errorWarnings"
})
@XmlRootElement(name = "ResultBlock")
public class ResultBlock {

    @XmlElement(name = "MatchSummary", required = true)
    protected MatchSummary matchSummary;
    @XmlElement(name = "ErrorWarnings", required = true)
    protected ErrorWarnings errorWarnings;

    /**
     * Gets the value of the matchSummary property.
     *
     * @return possible object is
     * {@link MatchSummary }
     */
    public MatchSummary getMatchSummary() {
        return matchSummary;
    }

    /**
     * Sets the value of the matchSummary property.
     *
     * @param value allowed object is
     *              {@link MatchSummary }
     */
    public void setMatchSummary(MatchSummary value) {
        this.matchSummary = value;
    }

    /**
     * Gets the value of the errorWarnings property.
     *
     * @return possible object is
     * {@link ErrorWarnings }
     */
    public ErrorWarnings getErrorWarnings() {
        return errorWarnings;
    }

    /**
     * Sets the value of the errorWarnings property.
     *
     * @param value allowed object is
     *              {@link ErrorWarnings }
     */
    public void setErrorWarnings(ErrorWarnings value) {
        this.errorWarnings = value;
    }

}

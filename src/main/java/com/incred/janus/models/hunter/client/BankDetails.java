package com.incred.janus.models.hunter.client;

import lombok.Data;

@Data
public class BankDetails {
    private String name;
    private String accountNo;
}

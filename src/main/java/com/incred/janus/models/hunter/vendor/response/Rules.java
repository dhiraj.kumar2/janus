//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0-b170531.0717 
//         See <a href="https://jaxb.java.net/">https://jaxb.java.net/</a> 
//         Any modifications to this file will be lost upon recompilation of the source schema. 
//         Generated on: 2019.12.30 at 02:16:11 PM IST 
//


package com.incred.janus.models.hunter.vendor.response;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected         content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.mclsoftware.co.uk/HunterII/WebServices}Rule" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="totalRuleCount" type="{http://www.w3.org/2001/XMLSchema}byte" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "rule"
})
@XmlRootElement(name = "Rules")
public class Rules {

    @XmlElement(name = "Rule")
    protected List<Rule> rule;
    @XmlAttribute(name = "totalRuleCount")
    protected Byte totalRuleCount;

    /**
     * Gets the value of the rule property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rule property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRule().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Rule }
     */
    public List<Rule> getRule() {
        if (rule == null) {
            rule = new ArrayList<Rule>();
        }
        return this.rule;
    }

    /**
     * Gets the value of the totalRuleCount property.
     *
     * @return possible object is
     * {@link Byte }
     */
    public Byte getTotalRuleCount() {
        return totalRuleCount;
    }

    /**
     * Sets the value of the totalRuleCount property.
     *
     * @param value allowed object is
     *              {@link Byte }
     */
    public void setTotalRuleCount(Byte value) {
        this.totalRuleCount = value;
    }

}

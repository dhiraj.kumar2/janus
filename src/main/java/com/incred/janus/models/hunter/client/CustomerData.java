package com.incred.janus.models.hunter.client;

import lombok.Data;

import java.math.BigInteger;
import java.util.Date;

@Data
public class CustomerData {
    private String pan;
    private String firstName;
    private String lastName;
    private String middleName;
    private String gender;
    private String maritalStatus;
    private BigInteger netMonthlySalary;
    private BigInteger mobile;
    private String email;
    private String aadhar;
    private Date dateOfBirth;
}

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0-b170531.0717 
//         See <a href="https://jaxb.java.net/">https://jaxb.java.net/</a> 
//         Any modifications to this file will be lost upon recompilation of the source schema. 
//         Generated on: 2019.12.20 at 06:55:28 PM IST 
//


package com.incred.janus.models.hunter.vendor.request;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigInteger;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected         content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}ADD"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}CTY"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}STE"/&gt;
 *         &lt;element ref="{urn:mclsoftware.co.uk:hunterII}PIN"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "add",
        "cty",
        "ste",
        "pin",
        "ctry"
})
@XmlRootElement(name = "MA_PMA")
public class MAPMA {

    @XmlElement(name = "ADD", required = true)
    protected String add;
    @XmlElement(name = "CTY", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String cty;
    @XmlElement(name = "STE", required = true)
    protected String ste;
    @XmlElement(name = "PIN", required = true)
    protected BigInteger pin;
    @XmlElement(name = "CTRY", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String ctry;

    public MAPMA(Builder builder) {
        this.add = builder.add;
        this.cty = builder.cty;
        this.ste = builder.ste;
        this.pin = builder.pin;
        this.ctry = builder.ctry;
    }

    public MAPMA() {
    }

    /**
     * Gets the value of the add property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getADD() {
        return add;
    }

    /**
     * Sets the value of the add property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setADD(String value) {
        this.add = value;
    }

    /**
     * Gets the value of the cty property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCTY() {
        return cty;
    }

    /**
     * Sets the value of the cty property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCTY(String value) {
        this.cty = value;
    }

    /**
     * Gets the value of the ste property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSTE() {
        return ste;
    }

    /**
     * Sets the value of the ste property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSTE(String value) {
        this.ste = value;
    }

    /**
     * Gets the value of the pin property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public BigInteger getPIN() {
        return pin;
    }

    /**
     * Sets the value of the pin property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setPIN(BigInteger value) {
        this.pin = value;
    }


    public static class Builder {
        private String add;
        private String cty;
        private String ste;
        private String ctry;
        private BigInteger pin;

        public Builder add(String add) {
            this.add = add;
            return this;
        }

        public Builder cty(String cty) {
            this.cty = cty;
            return this;
        }

        public Builder ste(String ste) {
            this.ste = ste;
            return this;
        }

        public Builder pin(BigInteger pin) {
            this.pin = pin;
            return this;
        }

        public Builder ctry(String ctry) {
            this.ctry = ctry;
            return this;
        }

        public Builder fromPrototype(MAPMA prototype) {
            add = prototype.add;
            cty = prototype.cty;
            ste = prototype.ste;
            pin = prototype.pin;
            ctry = prototype.ctry;
            return this;
        }

        public MAPMA build() {
            return new MAPMA(this);
        }
    }
}

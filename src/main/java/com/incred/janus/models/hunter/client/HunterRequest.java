package com.incred.janus.models.hunter.client;

import com.incred.janus.models.core.Request;
import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Introspected
public class HunterRequest extends Request {
    private String product;
    private String classification;
    private Date createdAt;
    private int loanAmount;
    private String branchLocation;
    private CustomerData primaryCustomerData;
    private EmploymentDetails employmentDetails;
    private Address presentAddress;
    private Address permanentAddress;
    private BankDetails bankDetails;
    private List<Reference> referenceList;
    private List<JointApplication> jointApplications;
}

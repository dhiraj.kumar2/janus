package com.incred.janus.models.hunter.client;

import com.incred.janus.models.core.Response;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HunterResponse extends Response {
    private String result;

    public HunterResponse(Boolean success, String result, String error) {
        super(success, error);
        this.result = result;
    }
}

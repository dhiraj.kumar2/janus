package com.incred.janus.models.perfios.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SOAPResponse {
    private int statusCode;
    private String response;
}

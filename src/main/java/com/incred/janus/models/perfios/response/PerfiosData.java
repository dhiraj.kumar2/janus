package com.incred.janus.models.perfios.response;

import com.incred.janus.models.perfios.report.Report;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PerfiosData {
    private String perfiosTxnId;
    private String clientTxnId;
    private Report report;
}

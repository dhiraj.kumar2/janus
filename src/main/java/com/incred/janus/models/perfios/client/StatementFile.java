
package com.incred.janus.models.perfios.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatementFile implements Serializable {

    private final static long serialVersionUID = -4485701039665937282L;
    @JsonProperty("FILE_KEY")
    private String fileKey;
    @JsonProperty("FILE_NAME")
    private String fileName;
    @JsonProperty("PASSWORD")
    private String password;
    @JsonProperty("PROTECTED")
    private boolean isProtected;
    private Map<String, Object> additionalProperties = new HashMap<>();
}

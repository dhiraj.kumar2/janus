
package com.incred.janus.models.perfios.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FinancialProfile implements Serializable {

    private final static long serialVersionUID = 3997067106938439527L;
    @JsonProperty("PENNY_TESTING_HISTORY")
    public List<PennyTestingHistory> pennyTestingHistoryList;
    @JsonProperty("BANK")
    private Bank bank;
    @JsonProperty("CUSTOMER_ID")
    private String customerId;
    @JsonProperty("EPOCH")
    private Long epoch;
    private Map<String, Object> additionalProperties = new HashMap<>();
}

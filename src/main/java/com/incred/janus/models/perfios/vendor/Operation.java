package com.incred.janus.models.perfios.vendor;

public enum Operation {
    START("start"),
    UPLOAD("upload"),
    UPLOAD_COMPLETE("upload-complete"),
    CHECK_STATUS("check-status"),
    RETRIEVE_DATA("retrieve-data"),
    INSTITUTIONS("Institutions");

    public String value;

    Operation(String value) {
        this.value = value;
    }
}

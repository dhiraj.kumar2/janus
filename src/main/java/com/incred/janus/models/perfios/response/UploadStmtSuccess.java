//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.11 at 04:20:06 PM IST 
//


package com.incred.janus.models.perfios.response;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accounts">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="account">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="accountPattern" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
 *                             &lt;element name="transactionEndDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="transactionStartDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="statementId" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accounts",
    "statementId"
})
@XmlRootElement(name = "Success")
public class UploadStmtSuccess {

    @XmlElement(required = true)
    protected UploadStmtSuccess.Accounts accounts;
    @XmlSchemaType(name = "unsignedInt")
    protected long statementId;

    /**
     * Gets the value of the accounts property.
     * 
     * @return
     *     possible object is
     *     {@link UploadStmtSuccess.Accounts }
     *     
     */
    public UploadStmtSuccess.Accounts getAccounts() {
        return accounts;
    }

    /**
     * Sets the value of the accounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link UploadStmtSuccess.Accounts }
     *     
     */
    public void setAccounts(UploadStmtSuccess.Accounts value) {
        this.accounts = value;
    }

    /**
     * Gets the value of the statementId property.
     * 
     */
    public long getStatementId() {
        return statementId;
    }

    /**
     * Sets the value of the statementId property.
     * 
     */
    public void setStatementId(long value) {
        this.statementId = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="account">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="accountPattern" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
     *                   &lt;element name="transactionEndDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="transactionStartDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "account"
    })
    public static class Accounts {

        @XmlElement(required = true)
        protected UploadStmtSuccess.Accounts.Account account;

        /**
         * Gets the value of the account property.
         * 
         * @return
         *     possible object is
         *     {@link UploadStmtSuccess.Accounts.Account }
         *     
         */
        public UploadStmtSuccess.Accounts.Account getAccount() {
            return account;
        }

        /**
         * Sets the value of the account property.
         * 
         * @param value
         *     allowed object is
         *     {@link UploadStmtSuccess.Accounts.Account }
         *     
         */
        public void setAccount(UploadStmtSuccess.Accounts.Account value) {
            this.account = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="accountPattern" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
         *         &lt;element name="transactionEndDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="transactionStartDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "accountPattern",
            "transactionEndDate",
            "transactionStartDate"
        })
        public static class Account {

            @XmlElement(required = true)
            @XmlSchemaType(name = "unsignedLong")
            protected BigInteger accountPattern;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar transactionEndDate;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar transactionStartDate;

            /**
             * Gets the value of the accountPattern property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getAccountPattern() {
                return accountPattern;
            }

            /**
             * Sets the value of the accountPattern property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setAccountPattern(BigInteger value) {
                this.accountPattern = value;
            }

            /**
             * Gets the value of the transactionEndDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getTransactionEndDate() {
                return transactionEndDate;
            }

            /**
             * Sets the value of the transactionEndDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setTransactionEndDate(XMLGregorianCalendar value) {
                this.transactionEndDate = value;
            }

            /**
             * Gets the value of the transactionStartDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getTransactionStartDate() {
                return transactionStartDate;
            }

            /**
             * Sets the value of the transactionStartDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setTransactionStartDate(XMLGregorianCalendar value) {
                this.transactionStartDate = value;
            }

        }

    }

}

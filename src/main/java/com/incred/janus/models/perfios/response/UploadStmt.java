package com.incred.janus.models.perfios.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UploadStmt {
    private int statusCode;
    private String message;
    private File file;
}


package com.incred.janus.models.perfios.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PennyTestingHistory implements Serializable {
    private final static long serialVersionUID = -1905621148454059342L;

    @JsonProperty("ACCOUNT_HOLDER_NAME")
    private String accountHolderName;
    @JsonProperty("ACCOUNT_NUM")
    private String accountNumber;
    @JsonProperty("EPOCH")
    private Long epoch;
    @JsonProperty("IFSC_CODE")
    private String ifscCide;
    @JsonProperty("PENNY_TESTING_RES")
    private String pennyTestingResult;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
}

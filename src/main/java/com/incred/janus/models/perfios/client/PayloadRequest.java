package com.incred.janus.models.perfios.client;

import com.incred.janus.models.perfios.vendor.Operation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PayloadRequest {
    private String perfiosTxnId;
    private String loanType;
    private Integer loanAmount;
    private Integer loanDuration;
    private String callbackUrl;
    private Operation operation;
    private String vendorId;
    private Integer perfiosCode;
    private String clientTxnId;
    private boolean scannedStmts;
    private String yearMonthFrom;
    private String yearMonthTo;
}

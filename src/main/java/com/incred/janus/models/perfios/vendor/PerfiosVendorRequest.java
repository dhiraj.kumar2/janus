package com.incred.janus.models.perfios.vendor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PerfiosVendorRequest {
    private String payload;
    private String signature;
    private String clientTxnId;

    public String queryString() {
        return "payload=" + URLEncoder.encode(payload, StandardCharsets.UTF_8) + "&signature=" + signature;
    }
}

package com.incred.janus.models.perfios.response;

import com.incred.janus.models.core.TransactionLog;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StartTxn {
    private int statusCode;
    private PerfiosError error;
    private String perfiosTxnId;
    private TransactionLog txnLog;
}

package com.incred.janus.models.perfios.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.incred.janus.models.core.Request;
import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Introspected
@JsonIgnoreProperties(ignoreUnknown = true)
public class PerfiosRequest extends Request {
    private FinancialProfile profile;
    private String loanType;
    private Integer loanAmount;
    private Integer loanDuration;
    private String bucketName;
    private boolean scannedStmts;
    private String yearMonthFrom;
    private String yearMonthTo;
}

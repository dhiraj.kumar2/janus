package com.incred.janus.models.perfios.client;

import com.incred.janus.models.core.Response;
import com.incred.janus.models.perfios.response.PerfiosData;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class PerfiosResponse extends Response {
    private PerfiosData data;

    public PerfiosResponse(PerfiosData data) {
        super(true);
        this.data = data;
    }

    public PerfiosResponse(Boolean success, String message) {
        super(success, message);
    }
}

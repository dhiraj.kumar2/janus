
package com.incred.janus.models.perfios.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountType implements Serializable {
    private final static long serialVersionUID = -5257421002709452580L;

    @JsonProperty("CODE")
    public String code;
    @JsonProperty("DESC")
    public String description;
    @JsonProperty("ID")
    public Long id;
    @JsonProperty("MANDATORY")
    public Long mandatory;
    @JsonProperty("P_CODE")
    public String pCode;
    @JsonProperty("P_TYPE")
    public String pType;
    @JsonProperty("RESIDENCE_TYPE")
    public String residenceType;
    @JsonProperty("STATUS")
    public String status;
    @JsonProperty("TYPE")
    public String type;
    @JsonProperty("UPLOAD_DOCUMENT")
    public Long uploadDocument;
}

package com.incred.janus.models.perfios.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UploadComplete {
    private int statusCode;
    private UploadCompleteSuccess success;
    private PerfiosError error;
}

package com.incred.janus.models.perfios.report;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Report {
    private CustomerInfo customerInfo;
    private SummaryInfo summaryInfo;
    private List<MonthWiseDetail> monthwiseDetails = new ArrayList<>();
    private List<StatementDetail> statementDetails = new ArrayList<>();
    private AccountDetails accountDetails;
    private RecurringExpenses recurringExpenses;
    private HighValueDebits highValueDebits;
    private HighValueCredits highValueCredits;
    private HighValueCreditsIncCash highValueCreditsIncCash;
    private List<AccountXNS> accountXns = new ArrayList<>();
    @JsonProperty("fCUAnalysis")
    private FCUAnalysis fCUAnalysis;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}

package com.incred.janus.models.perfios.report;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SummaryInfo {
    private double avgBalanceOf6Dates;
    private double avgDailyBalance;
    private double avgMonthlyExpense;
    private double avgMonthlyIncome;
    private double avgMonthlySurplus;
    private double closingBalance;
    private String expenseToIncomeRatio;
    private double highestExpenseInAMonth;
    private double highestIncomeInAMonth;
    private double highestInvestmentExpenseInAMonth;
    private double highestInvestmentIncomeInAMonth;
    private double highestSavingsInAMonth;
    private int inwardChqBounces;
    private int inwardECSBounces;
    private int inwardOtherBounces;
    private double lastMonthExpense;
    private double lastMonthIncome;
    private double lastMonthInvestmentExpense;
    private double lastMonthInvestmentIncome;
    private double lastMonthSavings;
    private double lowestExpenseInAMonth;
    private double lowestIncomeInAMonth;
    private double lowestInvestmentExpenseInAMonth;
    private double lowestInvestmentIncomeInAMonth;
    private double lowestSavingsInAMonth;
    private double maxBalance;
    private double maxCredit;
    private double maxDebit;
    private double minBalance;
    private double minCredit;
    private double minDebit;
    private int numberOfTransactions;
    private double openingBalance;
    private int outwardChqBounces;
    private int outwardECSBounces;
    private int outwardOtherBounces;
    private int salaryCredits;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

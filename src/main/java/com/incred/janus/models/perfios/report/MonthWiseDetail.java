package com.incred.janus.models.perfios.report;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MonthWiseDetail {
    private String month;
    private double maxBalance;
    private double minBalance;
    private double balanceOn1st;
    private double balanceOn5th;
    private double balanceOn10th;
    private double balanceOn15th;
    private double balanceOn20th;
    private double balanceOn25th;
    private double balanceOn30th;
    private double balAvgOf6Dates;
    private String expenseToIncomeRatio;
    private int inwardPaymentBounces;
    private double maxCredit;
    private double maxDebit;
    private double minCredit;
    private double minDebit;
    private int numberOfTransactions;
    private int outwardPaymentBounces;
    private int salaryCredits;
    private double totalCreditCardPayment;
    private double totalExpense;
    private double totalIncome;
    private double totalSurplus;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}

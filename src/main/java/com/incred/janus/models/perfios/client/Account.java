
package com.incred.janus.models.perfios.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account implements Serializable {
    private final static long serialVersionUID = -2414505687953586930L;

    @JsonProperty("ACCOUNT_HOLDER_NAME")
    private String accountHolderName;
    @JsonProperty("ACCOUNT_NUM")
    private String accountNumber;
    @JsonProperty("ACCOUNT_TYPE")
    private AccountType accountType;
    @JsonProperty("BANK_NAME")
    private BankDetails bankDetails;
    @JsonProperty("BANK_STATEMENT")
    private List<String> bankStatement;
    @JsonProperty("DATA_ENTRY_TYPE")
    private String dataEntryType;
    @JsonProperty("FILE_COLLECTION")
    private List<StatementFile> files;
    @JsonProperty("IFSC_CODE")
    private String ifscCode;
    @JsonProperty("OPERATING_SINCE")
    private String operatingSince;
    @JsonProperty("PENNY_TESTING_RES")
    private String pennyTestingResult;
}


package com.incred.janus.models.perfios.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BankDetails implements Serializable {
    private final static long serialVersionUID = 4380070958376592004L;

    @JsonProperty("CODE")
    private String code;
    @JsonProperty("DESC")
    private String desc;
    @JsonProperty("ID")
    private Long id;
    @JsonProperty("MANDATORY")
    private Long mandatory;
    @JsonProperty("P_CODE")
    private String pCode;
    @JsonProperty("P_TYPE")
    private String pType;
    @JsonProperty("PERFIOS_CODE")
    private Integer perfiosCode;
    @JsonProperty("RESIDENCE_TYPE")
    private String residenceType;
    @JsonProperty("STATUS")
    private String status;
    @JsonProperty("TYPE")
    private String type;
    @JsonProperty("UPLOAD_DOCUMENT")
    private Long uploadDocument;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
}

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.01.22 at 05:31:35 PM IST 
//


package com.incred.janus.models.perfios.vendor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InstitutionType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InstitutionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="addressAvailable" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="form26AsAvailable" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="institutionType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="itrVAvailable" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="originalStatementAvailable" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstitutionType", propOrder = {
        "addressAvailable",
        "form26AsAvailable",
        "id",
        "institutionType",
        "itrVAvailable",
        "name",
        "originalStatementAvailable"
})
public class InstitutionType {

    @XmlElement(required = true)
    protected String addressAvailable;
    @XmlElement(required = true)
    protected String form26AsAvailable;
    protected short id;
    @XmlElement(required = true)
    protected String institutionType;
    @XmlElement(required = true)
    protected String itrVAvailable;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected String originalStatementAvailable;

    /**
     * Gets the value of the addressAvailable property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAddressAvailable() {
        return addressAvailable;
    }

    /**
     * Sets the value of the addressAvailable property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAddressAvailable(String value) {
        this.addressAvailable = value;
    }

    /**
     * Gets the value of the form26AsAvailable property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getForm26AsAvailable() {
        return form26AsAvailable;
    }

    /**
     * Sets the value of the form26AsAvailable property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setForm26AsAvailable(String value) {
        this.form26AsAvailable = value;
    }

    /**
     * Gets the value of the id property.
     */
    public short getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     */
    public void setId(short value) {
        this.id = value;
    }

    /**
     * Gets the value of the institutionType property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getInstitutionType() {
        return institutionType;
    }

    /**
     * Sets the value of the institutionType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setInstitutionType(String value) {
        this.institutionType = value;
    }

    /**
     * Gets the value of the itrVAvailable property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getItrVAvailable() {
        return itrVAvailable;
    }

    /**
     * Sets the value of the itrVAvailable property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setItrVAvailable(String value) {
        this.itrVAvailable = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the originalStatementAvailable property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getOriginalStatementAvailable() {
        return originalStatementAvailable;
    }

    /**
     * Sets the value of the originalStatementAvailable property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setOriginalStatementAvailable(String value) {
        this.originalStatementAvailable = value;
    }

}

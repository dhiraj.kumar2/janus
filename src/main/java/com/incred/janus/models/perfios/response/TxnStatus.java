package com.incred.janus.models.perfios.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TxnStatus {
    private int statusCode;
    private TxnStatusSuccess success;
    private PerfiosError error;
}

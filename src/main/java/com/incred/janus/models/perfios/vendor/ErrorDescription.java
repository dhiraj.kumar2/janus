package com.incred.janus.models.perfios.vendor;

public enum ErrorDescription {

    E0001("Unable to validate documents"),
    E0002("Another application is running"),
    E0003("Sorry, you have entered an Invalid PAN"),
    E0004("Invalid Aadhaar"),
    E0005("Unable to validate pincode"),
    E0006("Invalid Document Type or Document Name"),
    E0007("Unable to validate office pincode"),
    E0008("No Customer Found for the Application"),
    E0009("Application doesn't exists"),
    E0010("Mandatory parameters are missing"),
    E0011("Invalid master references"),
    E0500("Unable to create customer login"),
    E0501("Unable to create customer financial"),
    E0502("Unable to create customer profile"),
    E0503("Unable to create application"),
    E0000("Oops, something went wrong"),
    E0013("MIME type is not acceptable"),
    E0014("Incorrect file type"),
    E0504("Customer profile can't be updated"),
    E0015("Unable to upload file"),
    E0505("Unable to update Application"),
    E0016("Personnel email id is missing."),
    E0017("PAN or AADHAAR exist with another mobile"),
    E0018("Unable to fetch bureau"),
    E0019("Unable to get decision"),
    E0020("Task not found"),
    E0021("Unable to initaite drawdown"),
    E0022("Age validation failed"),
    E0023("Aadhaar, PAN or Mobile is blacklisted"),
    E0024("Unable to generate cibil insight"),
    E0025("Unable to fetch customer profile"),
    E0026("Unable to get cibil insight"),
    E0027("Dedupe failed"),
    E0028("Unable to initiate transaction"),
    E0029("Unable to fetch transaction"),
    E0030("Unable to fetch decision"),
    E0031("Unable to fetch customer financial"),
    E0032("Banking data not found"),
    E0033("Banking data is insufficient"),
    E0034("Credit Vidya data not found"),
    E0035("Credit Vidya Data is insufficient"),
    E0036("We are not operational in your location currently."),
    E0037("We are not providing service in this area"),
    E0038("Unable to update customer financial"),
    E0039("Bank transcription not supported by Perfios"),
    E0040("Please enter a valid pin code"),
    E0041("Employment Type verification failed"),
    E0042("Customer has a recent incomplete application with InCred"),
    E0043("Customer has a recent rejected application with InCred"),
    E0044("Customer has a recent loan disbursed from InCred"),
    E0045("Customer is delinquent on loan disbursed from InCred"),
    E0046("Customer has an inprogress application with InCred");

    public String description;

    ErrorDescription(String description) {
        this.description = description;
    }
}

package com.incred.janus.models.karza.vendor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KarzaRequest implements Serializable {

    private final static long serialVersionUID = -5400922505184303826L;
    @JsonProperty("pan")
    private String pan;
    @JsonProperty("consent")
    private String consent;
}
package com.incred.janus.models.karza.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "PanNumber",
        "Name",
        "LastUpdate",
        "STATUS",
        "StatusDescription"
})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PanData implements Serializable {

    private final static long serialVersionUID = 141970107764105703L;
    @JsonProperty("PanNumber")
    private String panNumber;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("LastUpdate")
    private String lastUpdate;
    @JsonProperty("STATUS")
    private String status;
    @JsonProperty("StatusDescription")
    private String statusDescription;
}
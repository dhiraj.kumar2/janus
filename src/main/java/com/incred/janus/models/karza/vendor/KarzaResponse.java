package com.incred.janus.models.karza.vendor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class KarzaResponse implements Serializable {

    private final static long serialVersionUID = 4752058637020797508L;
    @JsonProperty("result")
    private Result result;
    @JsonProperty("request_id")
    private String requestId;
    @JsonProperty("status-code")
    private String statusCode;
    @JsonProperty("error")
    private String error;
}
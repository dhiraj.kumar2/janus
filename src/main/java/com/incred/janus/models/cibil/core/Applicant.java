package com.incred.janus.models.cibil.core;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.incred.janus.models.cibil.request.application.AccountType;
import com.incred.janus.models.cibil.request.application.AddressType;
import com.incred.janus.models.cibil.request.application.IdentifierType;
import com.incred.janus.models.cibil.request.application.TelephoneType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Applicant {
    @NotNull
    private String applicantType;
    @NotNull
    private String applicantFirstName;
    @NotNull
    private String applicantMiddleName;
    @NotNull
    private String applicantLastName;
    private @NotNull String dateOfBirth;
    @NotNull
    private String gender;
    private String emailAddress;
    private String companyName;
    @JsonAlias({"identifier", "identifiers"})
    private List<IdentifierType> identifier;
    private List<TelephoneType> telephones;
    @Min(1)
    private List<AddressType> addresses;
    @NotNull
    private List<AccountType> accounts;

    @NotNull
    private Nominee nominee;
}

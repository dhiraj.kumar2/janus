package com.incred.janus.models.cibil.core;

import com.incred.janus.models.cibil.request.applicationData.ApplicationDataType;
import com.incred.janus.models.core.Request;
import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Introspected
public class CIBILRequest extends Request {
    @NotNull
    private ApplicationDataType applicationData;
    @NotNull
    private Applicant applicant;
}
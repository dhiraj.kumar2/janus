package com.incred.janus.models.cibil.core;

import com.incred.janus.models.cibil.response.CreditReport;
import com.incred.janus.models.core.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CIBILResponse extends Response {
    private CreditReport creditReport;
    private CreditReport secondaryCreditReport;
    private String cibilAppId;
}

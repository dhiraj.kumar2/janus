package com.incred.janus.models.cibil.core;

import com.incred.janus.models.core.Request;
import com.incred.janus.models.core.Response;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CIBILReportResponse extends Response {
    private String html;
    private String docId;

    public CIBILReportResponse(String html, boolean success, String error) {
        super(success, error);
        this.html = html;
    }
}

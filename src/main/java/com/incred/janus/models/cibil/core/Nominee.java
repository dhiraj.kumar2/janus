package com.incred.janus.models.cibil.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Nominee {
    private String nomineeRelation;
    private String nomineeName;

    private String memberRelationType3;
    private String memberRelationName3;
    private String memberRelationType2;
    private String memberRelationName2;
    private String memberRelationType1;
    private String memberRelationName1;

    private String keyPersonRelation;
    private String keyPersonName;

    private String memberOtherId3;
    private String memberOtherId3Type;
    private String memberOtherId2;
    private String memberOtherId2Type;
    private String memberOtherId1;
    private String memberOtherId1Type;
}

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.01.21 at 01:52:15 PM IST 
//


package com.incred.janus.models.cibil.request.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TelephoneType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TelephoneType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TelephoneExtension" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TelephoneNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="TelephoneType" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TelephoneType", propOrder = {
    "telephoneExtension",
    "telephoneNumber",
    "telephoneType"
})
public class TelephoneType {

    @XmlElement(name = "TelephoneExtension", required = true)
    private String telephoneExtension;
    @XmlElement(name = "TelephoneNumber")
    private long telephoneNumber;
    @XmlElement(name = "TelephoneType")
    private String telephoneType;

    /**
     * Gets the value of the telephoneExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelephoneExtension() {
        return telephoneExtension;
    }

    /**
     * Sets the value of the telephoneExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelephoneExtension(String value) {
        this.telephoneExtension = value;
    }

    /**
     * Gets the value of the telephoneNumber property.
     * 
     */
    public long getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * Sets the value of the telephoneNumber property.
     * 
     */
    public void setTelephoneNumber(long value) {
        this.telephoneNumber = value;
    }

    /**
     * Gets the value of the telephoneType property.
     * 
     */
    public String getTelephoneType() {
        return telephoneType;
    }

    /**
     * Sets the value of the telephoneType property.
     * 
     */
    public void setTelephoneType(String value) {
        this.telephoneType = value;
    }

}

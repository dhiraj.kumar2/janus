package com.incred.janus.models.cibil.core;

import com.incred.janus.models.core.Request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CIBILReportRequest extends Request {
    @NotNull
    private String appId;
}

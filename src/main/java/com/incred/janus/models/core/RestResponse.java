package com.incred.janus.models.core;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.http.HttpHeaders;

@Data
@AllArgsConstructor
public class RestResponse {
    private String body;
    private int code;
    private HttpHeaders httpHeaders;
}

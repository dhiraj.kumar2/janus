package com.incred.janus.models.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "transaction_log")
public class TransactionLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String vendor;
    private String sender;
    private String platform;
    private String transactionId;
    private String status;
    private String applicationId;
    private String customerId;
    private Long retryCount;
    private String input;
    private String output;
    private String category;
    private Date createdAt;
    private Date updatedAt;
}

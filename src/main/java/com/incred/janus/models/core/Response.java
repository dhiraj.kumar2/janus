package com.incred.janus.models.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Response {
    protected Boolean success;
    protected String error;
    protected String transactionId;
    protected String errorCode;

    private String statementErrorCode;
    @JsonIgnore
    protected boolean cacheHit;

    public Response(Boolean success, String error) {
        this.success = success;
        this.error = error;
    }

    public Response(boolean success) {
        this.success = success;
    }
}

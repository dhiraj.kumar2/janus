package com.incred.janus.models.core;

import com.incred.janus.util.RandomUtil;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Introspected
public class Request {
    @NotNull
    protected String platform;
    @NotNull
    protected String applicationId;
    @NotNull
    protected String sender;
    private String transactionId = RandomUtil.unique();
}

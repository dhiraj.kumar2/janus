package com.incred.janus.models.authbridge.vendor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthBridgeVendorRequestParam {
    private String transID;
    private String docType;
    private String docNumber;
}

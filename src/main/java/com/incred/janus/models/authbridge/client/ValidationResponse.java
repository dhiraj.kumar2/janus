package com.incred.janus.models.authbridge.client;

import com.incred.janus.models.core.Response;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ValidationResponse extends Response {
    private Object data;

    public ValidationResponse(Boolean success, String message, Object data) {
        super(success, message);
        this.data = data;
    }
}

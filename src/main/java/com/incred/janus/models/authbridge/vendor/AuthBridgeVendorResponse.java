package com.incred.janus.models.authbridge.vendor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthBridgeVendorResponse {

    private String responseData;
    @JsonProperty("status")
    private String status;
    @JsonProperty("msg")
    private String message;
}

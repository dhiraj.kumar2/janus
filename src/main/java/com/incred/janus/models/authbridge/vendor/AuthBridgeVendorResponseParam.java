package com.incred.janus.models.authbridge.vendor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthBridgeVendorResponseParam {
    private String status;
    @JsonProperty("msg")
    private Object message;
    private ArrayList<HashMap<String, Object>> data;
}

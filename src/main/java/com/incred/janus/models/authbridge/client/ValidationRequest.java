package com.incred.janus.models.authbridge.client;

import com.incred.janus.models.core.Request;
import io.micronaut.core.annotation.Introspected;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Introspected
public class ValidationRequest extends Request {
    @NotNull
    private String docType;
    @NotNull
    private String docNumber;
}

package com.incred.janus.services;

import com.incred.janus.models.cibil.core.CIBILReportRequest;
import com.incred.janus.models.cibil.core.CIBILReportResponse;
import com.incred.janus.models.cibil.core.CIBILRequest;
import com.incred.janus.models.cibil.core.CIBILResponse;
import com.incred.janus.models.cibil.request.application.ApplicantsType;
import com.incred.janus.models.cibil.request.applicationData.ApplicationDataType;
import com.incred.janus.models.cibil.response.error.ErrorResponse;
import com.incred.janus.models.core.TransactionLog;
import com.incred.janus.util.*;
import io.micrometer.core.instrument.Timer;
import io.micronaut.context.annotation.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collections;
import java.util.HashMap;

@Singleton
public class CIBILService {
    @Value("${cibil.userId}")
    String userId;
    @Value("${cibil.password}")
    String password;
    @Value("${cibil.url}")
    String url;
    @Value("${cibil.action.executeXMLString}")
    String executeXMLString;
    @Value("${cibil.action.retrieveDocumentMetaDataXMLString}")
    String retrieveDocumentMetaDataXMLString;
    @Value("${cibil.action.downloadDocument}")
    String downloadDocument;
    @Value("${cibil.memberCode}")
    String memberCode;
    @Value("${cibil.responsePassword}")
    String responsePassword;
    private static final Logger log = LoggerFactory.getLogger(PerfiosService.class);

    @Inject
    MetricsUtil metricsUtil;

    @Inject
    CIBILUtil util;

    @Inject
    SoapUtil soapUtil;

    @Inject
    TransactionUtil transactionUtil;

    @Inject
    DynamoDBUtil dbUtil;

    public CIBILResponse request(CIBILRequest cibilRequest) {
        try {
            TransactionLog transactionLog = transactionUtil.startCibilTxn(cibilRequest);
            MetricsUtil.init();
            Timer.Sample sample = Timer.start(MetricsUtil.registry);
            cibilRequest.getApplicationData().setMemberCode(memberCode);
            cibilRequest.getApplicationData().setPassword(responsePassword);
            log.info("Requested CIBIL Meta For " + cibilRequest.getApplicant().getApplicantFirstName());
            if ((cibilRequest.getApplicant().getIdentifier() == null || cibilRequest.getApplicant().getIdentifier()
                                                                                    .size() == 0)
                    && (cibilRequest.getApplicant().getTelephones() == null || cibilRequest.getApplicant()
                                                                                           .getTelephones()
                                                                                           .size() == 0)) {
                sample.stop(MetricsUtil.registry.timer("cibil.timer", "status", "invalid_input"));
                CIBILResponse cibilResponse = new CIBILResponse();
                cibilResponse.setTransactionId(cibilRequest.getTransactionId());
                cibilResponse.setSuccess(false);
                cibilResponse.setError("Either Identifier or Telephone is mandatory.");
                transactionUtil.updateTransaction(cibilResponse, transactionLog);
                return cibilResponse;
            }
            try {
                ApplicantsType applicantsType = util.toXMLApplicantTypes(cibilRequest);
                ApplicationDataType applicationDataType = cibilRequest.getApplicationData();
                HashMap<String, String> headers = new HashMap<>();
                headers.put("soapAction", executeXMLString);
                String soapResponse = soapUtil
                        .post(url, headers, util.getCIBILXMLReq(applicantsType, applicationDataType, userId, password));

                if (util.isSuccess(soapResponse)) {
                    CIBILResponse cibilResponse = util.toSuccessPOJO(soapResponse);
                    cibilResponse.setTransactionId(cibilRequest.getTransactionId());
                    sample.stop(MetricsUtil.registry.timer("cibil.timer", "status", "success"));
                    transactionUtil.updateTransaction(cibilResponse, transactionLog);
                    storeInDynamo(cibilRequest, cibilResponse);
                    return cibilResponse;
                } else {
                    ErrorResponse errorResponse = util.toErrorPOJO(soapResponse);
                    CIBILResponse cibilResponse = new CIBILResponse();
                    cibilResponse.setTransactionId(cibilRequest.getTransactionId());
                    cibilResponse.setSuccess(false);
                    cibilResponse.setError(errorResponse.getErrorDescription());
                    sample.stop(MetricsUtil.registry.timer("cibil.timer", "status", "cibil_failure"));
                    transactionUtil.updateTransaction(cibilResponse, transactionLog);
                    return cibilResponse;
                }
            } catch (Exception e) {
                log.error("Error processing request " + cibilRequest.getApplicant().getApplicantFirstName() + e
                        .getMessage());
                CIBILResponse cibilResponse = new CIBILResponse();
                cibilResponse.setSuccess(false);
                cibilResponse.setTransactionId(cibilRequest.getTransactionId());
                cibilResponse.setError(e.getMessage());
                sample.stop(MetricsUtil.registry.timer("cibil.timer", "status", "technical_failure"));
                transactionUtil.updateTransaction(cibilResponse, transactionLog);
                return cibilResponse;
            }
        } catch (Exception e) {
            log.error("Error processing request " + e.getMessage());
            CIBILResponse cibilResponse = new CIBILResponse();
            cibilResponse.setSuccess(false);
            cibilResponse.setTransactionId(cibilRequest.getTransactionId());
            cibilResponse.setError(e.getMessage());
            return cibilResponse;
        }
    }

    public CIBILReportResponse report(CIBILReportRequest cibilReportRequest) {
        MetricsUtil.init();
        Timer.Sample sample = Timer.start(MetricsUtil.registry);
        log.info("Requested CIBIL Report For " + cibilReportRequest.getAppId());
        String appId = cibilReportRequest.getAppId();
        HashMap<String, String> headers = new HashMap<>();
        headers.put("soapAction", retrieveDocumentMetaDataXMLString);
        try {
            String soapResponse = soapUtil.post(url, headers, util.getDocXMLReq(appId, userId, password));
            String docId;


            docId = util.getDocId(soapResponse);
            if (docId == null) {
                sample.stop(MetricsUtil.registry.timer("cibil.report.timer", "status", "invalid_input"));
                return new CIBILReportResponse(null, false, "Invalid AppId");
            }

            headers.put("soapAction", downloadDocument);
            soapResponse = soapUtil.post(url, headers, util.getDocBufferXMLReq(appId, docId, userId, password));

            String html = util.getHTMLBuffer(soapResponse);
            if (html == null) {
                sample.stop(MetricsUtil.registry.timer("cibil.report.timer", "status", "cibil_failure"));
                return new CIBILReportResponse(null, false, "Invalid DocId");
            } else {
                CIBILReportResponse cibilReportResponse = new CIBILReportResponse(html, true, null);
                cibilReportResponse.setDocId(docId);
                cibilReportResponse.setTransactionId(cibilReportRequest.getTransactionId());
                sample.stop(MetricsUtil.registry.timer("cibil.report.timer", "status", "success"));
                return cibilReportResponse;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error Responding CIBIL Report For " + cibilReportRequest.getAppId() + " : " + e.getMessage());
            sample.stop(MetricsUtil.registry.timer("cibil.report.timer", "status", "technical_failure"));
            return new CIBILReportResponse(null, false, e.getMessage());
        }
    }

    private void storeInDynamo(CIBILRequest request, CIBILResponse response) {
        try {
            log.info("Storing in Dynamo : " + request.getApplicationId());
            if (response.getSuccess() && response.getError() == null && !response.isCacheHit()) {
                dbUtil.put(DynamoDBUtil.BUREAU, "APPLICATION_ID", request.getApplicationId(),
                           Collections.singletonMap("RESPONSE", response));
            }
        } catch (Exception e) {
            log.error("Unable to write to DB : " + e.getMessage());
        }
    }
}

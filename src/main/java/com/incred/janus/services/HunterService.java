package com.incred.janus.services;

import com.incred.janus.models.core.TransactionLog;
import com.incred.janus.models.hunter.client.*;
import com.incred.janus.models.hunter.vendor.request.*;
import com.incred.janus.models.hunter.vendor.response.Errors;
import com.incred.janus.models.hunter.vendor.response.MatchResponse;
import com.incred.janus.util.DynamoDBUtil;
import com.incred.janus.util.HunterUtil;
import com.incred.janus.util.SoapUtil;
import com.incred.janus.util.TransactionUtil;
import io.micronaut.context.annotation.Value;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

@Singleton
public class HunterService {
    private static final Logger log = LoggerFactory.getLogger(HunterService.class);

    @Value("${hunter.userName}")
    String userName;
    @Value("${hunter.password}")
    String password;
    @Value("${hunter.schemeSetID}")
    int schemeSetID;
    @Value("${hunter.hunterCustomerId}")
    int hunterCustomerId;
    @Value("${hunter.url}")
    String url;

    @Inject
    HunterUtil util;

    @Inject
    SoapUtil soapUtil;

    @Inject
    TransactionUtil transactionUtil;

    @Inject
    DynamoDBUtil dbUtil;

    public HunterResponse makeHunterRequest(HunterRequest hunterRequest) {
        HunterResponse hunterResponse;
        try {
            TransactionLog transactionLog = transactionUtil.startHunterTxn(hunterRequest);
            SUBMISSION submission = hunterRequestToFinalRequest(hunterRequest);
            log.debug("Parsing request to hunter request");
            String xmlBody = util.toXML(submission);
            String requestBody = util.getHunterXML(xmlBody, userName, password, schemeSetID, hunterCustomerId);
            requestBody = cleanupRequest(requestBody);
            String response = soapUtil.post(url, new HashMap<>(), requestBody);
            MatchResponse matchResponse = util.toPOJO(response);
            hunterResponse = parseResponse(matchResponse);
            transactionUtil.updateTransaction(hunterResponse, transactionLog);
            storeInDynamo(hunterRequest, hunterResponse);
        } catch (Exception e) {
            hunterResponse = new HunterResponse(false,null, "Error while processing request : " + e.getMessage());
        }

        return hunterResponse;
    }

    @NotNull
    private HunterResponse parseResponse(MatchResponse matchResponse) {
        HunterResponse response;
        if (matchResponse == null || matchResponse.getMatchResult() == null || matchResponse.getMatchResult()
                                                                                            .getResultBlock() == null) {
            response = new HunterResponse(false, null, "Error while calling hunter");
        } else {
            Errors errors = matchResponse.getMatchResult().getResultBlock().getErrorWarnings().getErrors();
            if (errors != null && errors.getErrorCount().intValue() > 0) {
                response = new HunterResponse(false, null, errors.getValue());
            } else if (matchResponse.getMatchResult().getResultBlock().getMatchSummary().getMatches().intValue() == 0) {
                response = new HunterResponse(true, "No Match", null);
            } else {
                response = new HunterResponse(true, "Match", null);
            }
        }
        return response;
    }

    @NotNull
    private String cleanupRequest(String requestBody) {
        return requestBody.replaceAll(" {2}", "")
                          .replaceAll(" \n", " ")
                          .replaceAll("\n", "")
                          .replaceAll("\r", "")
                          .replaceAll("> <", "><");
    }

    public SUBMISSION hunterRequestToFinalRequest(@NotNull HunterRequest hunterRequest) {
        SUBMISSION submission = new SUBMISSION();
        submission.setIDENTIFIER(hunterRequest.getApplicationId());
        submission.setPRODUCT(hunterRequest.getProduct());
        submission.setCLASSIFICATION(hunterRequest.getClassification());
        submission.setDATE(util.dateToXMLGregorianCalenderDate(new Date()));
        submission.setAPPDTE(util.dateToXMLGregorianCalenderDate(hunterRequest.getCreatedAt()));
        submission.setAPPVAL(hunterRequest.getLoanAmount());
        submission.setBRNCH_RGN(hunterRequest.getBranchLocation());
        MA mainApplicant = createMainApplicant(hunterRequest);
        submission.setMA(mainApplicant);
        addJointApplications(hunterRequest,submission);
        addReferences(hunterRequest, submission);
        return submission;
    }

    @NotNull
    private MA createMainApplicant(@NotNull HunterRequest hunterRequest) {
        MA mainApplicant = new MA();
        CustomerData primaryCustomerData = hunterRequest.getPrimaryCustomerData();
        addBasicDetails(mainApplicant, primaryCustomerData);
        addAddresses(hunterRequest, mainApplicant);
        addEmploymentDetails(hunterRequest, mainApplicant, primaryCustomerData);
        addIdDetails(mainApplicant, primaryCustomerData);
        addBankDetails(hunterRequest, mainApplicant);
        return mainApplicant;
    }

    private void addIdDetails(@NotNull MA mainApplicant, @NotNull CustomerData primaryCustomerData) {
        mainApplicant.setMAID(new MAID.Builder().doctyp(
                BigInteger.valueOf(primaryCustomerData.getAadhar() != null ? 72 : 32))
                                                .docno(primaryCustomerData.getAadhar() != null ? primaryCustomerData
                                                        .getAadhar() : primaryCustomerData.getPan()).build());
    }

    private void addBankDetails(@NotNull HunterRequest hunterRequest, @NotNull MA mainApplicant) {
        mainApplicant.setMABNK(new MABNK.Builder().accno(hunterRequest.getBankDetails().getAccountNo())
                                                  .bnknm(hunterRequest.getBankDetails().getName()).build());
    }

    private void addBasicDetails(@NotNull MA mainApplicant, @NotNull CustomerData primaryCustomerData) {
        mainApplicant.setPAN(primaryCustomerData.getPan());
        mainApplicant.setFSTNME(primaryCustomerData.getFirstName());
        mainApplicant.setLSTNME(primaryCustomerData.getLastName());
        mainApplicant.setMIDNME(primaryCustomerData.getMiddleName());
        //mainApplicant.setDOB(dateToXMLGregorianCalenderDate(primaryCustomerData.getDateOfBirth()).toXMLFormat());
        mainApplicant.setGNDR(BigInteger.valueOf(util.genderToCode(primaryCustomerData.getGender())));
        mainApplicant.setMARSTT(BigInteger.valueOf(util.maritalStatusToCode(primaryCustomerData.getMaritalStatus())));
        mainApplicant.setINC(primaryCustomerData.getNetMonthlySalary());
        mainApplicant.setMAHT(new TELNO(primaryCustomerData.getMobile()));
        mainApplicant.setMAMT(new TELNO(primaryCustomerData.getMobile()));
        mainApplicant.setMAEMA(new EMAADD(primaryCustomerData.getEmail()));
    }

    private void addEmploymentDetails(@NotNull HunterRequest hunterRequest, @NotNull MA mainApplicant, @NotNull CustomerData primaryCustomerData) {
        EmploymentDetails employmentDetails = hunterRequest.getEmploymentDetails();
        mainApplicant.setMAEMP(new MAEMP.Builder().orgnme(employmentDetails.getCompanyName()).maempad(
                new MAEMPAD.Builder().add(employmentDetails.getAddress().getAddress())
                                     .cty(employmentDetails.getAddress().getCity())
                                     .ste(employmentDetails.getAddress().getState())
                                     .ctry(employmentDetails.getAddress().getCountry())
                                     .pin(employmentDetails.getAddress().getPinCode()).build())
                                                  .maempbt(new TELNO(primaryCustomerData.getMobile())).build());
    }

    private void addAddresses(@NotNull HunterRequest hunterRequest, @NotNull MA mainApplicant) {
        Address presentAddress = hunterRequest.getPresentAddress();
        mainApplicant.setMACA(new MACA.Builder().add(presentAddress.getAddress()).cty(presentAddress.getCity())
                                                .ste(presentAddress.getState()).ctry(presentAddress.getCountry())
                                                .pin(presentAddress.getPinCode()).build());
        Address permanentAddress = hunterRequest.getPermanentAddress();
        mainApplicant.setMAPMA(new MAPMA.Builder().add(permanentAddress.getAddress()).cty(permanentAddress.getCity())
                                                  .ste(permanentAddress.getState()).ctry(permanentAddress.getCountry())
                                                  .pin(permanentAddress.getPinCode()).build());
    }

    private void addReferences(@NotNull HunterRequest hunterRequest, SUBMISSION submission) {
        hunterRequest.getReferenceList().forEach(reference -> submission.getRF().add(new RF.Builder()
                                                                                             .fstnme(reference
                                                                                                             .getName())
                                                                                             .rfmt(new TELNO(reference
                                                                                                                     .getMobile()))
                                                                                             .build()));
    }

    private void addJointApplications(@NotNull HunterRequest hunterRequest, SUBMISSION submission) {
        if (hunterRequest.getJointApplications() != null) {
            hunterRequest.getJointApplications().forEach(jointApplication -> submission.getJA().add(new JA.Builder()
                    .fstnme(jointApplication.getFirstName())
                    .pan(jointApplication.getPan())
                    .gndr(jointApplication.getGender())
                    .marstt(jointApplication.getMaritalStatus())
                    .inc((jointApplication.getNetMonthlySalary()))
                    .jaca(new JACA.Builder()
                            .add(jointApplication.getPresentAddress().getAddress())
                            .cty(jointApplication.getPresentAddress().getCity())
                            .ste(jointApplication.getPresentAddress().getState())
                            .ctry(jointApplication.getPresentAddress().getCountry())
                            .pin(jointApplication.getPresentAddress().getPinCode())
                            .build())
                    .jaid(new JAID.Builder()
                            .doctyp(BigInteger.valueOf(jointApplication.getAadhar() != null ? 72 : 32))
                            .docno(jointApplication.getAadhar() != null ? jointApplication.getAadhar() : jointApplication.getPan())
                            .build())
                    .build())
            );
        }
    }

    private void storeInDynamo(HunterRequest request, HunterResponse response) {
        try {
            if (response.getSuccess() && response.getError() == null && !response.isCacheHit()) {
                dbUtil.put(DynamoDBUtil.HUNTER, "APPLICATION_ID", request.getApplicationId(),
                           Collections.singletonMap("RESPONSE", response));
            }
        } catch (Exception e) {
            log.error("Unable to write to DB : " + e.getMessage());
        }
    }
}

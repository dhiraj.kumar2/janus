package com.incred.janus.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.incred.janus.core.ExceptionUtil;
import com.incred.janus.models.core.TransactionLog;
import com.incred.janus.models.perfios.client.*;
import com.incred.janus.models.perfios.report.Report;
import com.incred.janus.models.perfios.response.*;
import com.incred.janus.models.perfios.vendor.ErrorCodes;
import com.incred.janus.models.perfios.vendor.ErrorDescription;
import com.incred.janus.models.perfios.vendor.Operation;
import com.incred.janus.util.*;
import io.micrometer.core.instrument.Timer;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.HttpStatus;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton
public class PerfiosService {

    private static final Logger LOG = LoggerFactory.getLogger(PerfiosService.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final String SUCCESS = "SUCCESS";
    private static final String AVAILABLE = "AVAILABLE";
    private static final String COMPLETED = "COMPLETED";
    private static final String PENDING = "PENDING";
    private static final String NOT_AVAILABLE = "NOTAVAILABLE";

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String FORM_URLENCODED = "application/x-www-form-urlencoded";

    static {
        MetricsUtil.init();
    }

    @Inject
    private PerfiosUtil perfiosUtil;
    @Inject
    private RestUtil client;
    @Inject
    private S3Util s3Util;
    @Inject
    private SoapUtil soapUtil;
    @Inject
    private TransactionUtil txnUtil;

    @Value("${perfios.reportFormat}")
    private String reportFormat;
    @Value("${perfios.email}")
    private String email;
    @Value("${perfios.returnURL}")
    private String returnURL;
    @Value("${perfios.timeout}")
    private int timeout;
    @Value("${perfios.numRetry}")
    private int numRetry;

    public Mono<PerfiosResponse> start(PerfiosRequest request) throws Exception {
        LOG.info("Received perfios request : " + MAPPER.writeValueAsString(request));

        if (request == null || request.getProfile() == null) {
            return Mono.just(new PerfiosResponse(false,
                    ErrorDescription.valueOf(ErrorCodes.INSUFFICIENT_BANKING_DATA.value).description));
        }

        List<Account> accounts = request.getProfile().getBank().getAccounts();
        boolean bankAccountsWithNoPerfiosCode = accounts.stream().anyMatch(
                account -> account.getBankDetails().getCode().isEmpty()
                        || account.getBankDetails().getCode().equalsIgnoreCase("na"));
        if (bankAccountsWithNoPerfiosCode) {
            return Mono.just(new PerfiosResponse(false,
                    ErrorDescription.valueOf(ErrorCodes.BANKING_TRANSCRIPTION_ERROR.value).description));
        }
        boolean fileDoesNotExist = accounts.stream().map(Account::getFiles).flatMap(Collection::stream)
                .map(StatementFile::getFileKey)
                .anyMatch(key -> !s3Util.exists(request.getBucketName(), key));
        if (fileDoesNotExist) {
            return Mono.just(new PerfiosResponse(false,
                    ErrorDescription.valueOf(ErrorCodes.BANKING_DATA_MISSING_ERROR.value).description));
        }

        String clientTxnId = request.getTransactionId();
        AtomicReference<String> perfiosTxnId = new AtomicReference<>("");
        AtomicReference<TransactionLog> txnLog = new AtomicReference<>(new TransactionLog());

        return startTxn(accounts.get(0), request)
                .map(startTxn -> {
                    perfiosTxnId.set(startTxn.getPerfiosTxnId());
                    txnLog.set(startTxn.getTxnLog());

                    if (startTxn.getStatusCode() == HttpStatus.OK.getCode()) {
                        return uploadAllStmt(accounts.get(0), perfiosTxnId.get(), request.getBucketName());
                    } else {
                        return Mono.just(UploadAllStmt.builder().statusCode(startTxn.getStatusCode())
                                .error(startTxn.getError()).build());
                    }
                }).flatMap(Function.identity()).switchIfEmpty(Mono.defer(Mono::empty))
                .map(uploadAllStmt -> {
                    if (uploadAllStmt.getStatusCode() == HttpStatus.OK.getCode()) {
                        return uploadComplete(perfiosTxnId.get(), clientTxnId, txnLog.get());
                    } else {
                        return Mono.just(UploadComplete.builder().statusCode(uploadAllStmt.getStatusCode())
                                .error(uploadAllStmt.getError()).build());
                    }
                }).flatMap(Function.identity()).switchIfEmpty(Mono.defer(Mono::empty))
                .map(uploadComplete -> {
                    if (uploadComplete.getStatusCode() == HttpStatus.OK.getCode() ||
                            uploadComplete.getStatusCode() == HttpStatus.ACCEPTED.getCode()) {
                        return pollForData(perfiosTxnId.get(), clientTxnId, txnLog.get());
                    } else {
                        PerfiosResponse perfiosResp = new PerfiosResponse(false, uploadComplete.getError().getMessage());
                        perfiosResp.setErrorCode(uploadComplete.getError().getCode());
                        perfiosResp.setStatementErrorCode(uploadComplete.getError().getStatementErrorCode());
                        return Mono.just(perfiosResp);
                    }
                }).flatMap(Function.identity()).switchIfEmpty(Mono.defer(Mono::empty));
    }

    private Mono<StartTxn> startTxn(Account account, PerfiosRequest perfiosReq) {
        PayloadRequest request = perfiosUtil.getStartPerfiosRequest(perfiosReq, account.getBankDetails().getPerfiosCode());
        LOG.info("starting perfios txn {} {}", System.currentTimeMillis(), perfiosReq.getApplicationId());

        Timer.Sample sample = Timer.start(MetricsUtil.registry);
        return perfiosUtil.getSOAPResponse(request, soapUtil)
                .map(ExceptionUtil.wrapper(soapResp -> {
                    if (soapResp.getStatusCode() == HttpStatus.OK.getCode()) {
                        LOG.info("perfios txn started {} {}", System.currentTimeMillis(), perfiosReq.getApplicationId());
                        sample.stop(MetricsUtil.registry.timer("bank_transcription.start.timer", "vendor", PerfiosUtil.PERFIOS));

                        try {
                            StartTxnSuccess startTxnSuccess = perfiosUtil.toPOJO(soapResp.getResponse(), StartTxnSuccess.class);

                            TransactionLog txnLog = txnUtil.startPerfiosTxn(perfiosReq);

                            return StartTxn.builder().statusCode(soapResp.getStatusCode())
                                    .perfiosTxnId(startTxnSuccess.getPerfiosTransactionId())
                                    .txnLog(txnLog).build();
                        } catch (Exception e) {
                            LOG.error("Error in parsing request to string {} {}", perfiosReq.getApplicationId(), ExceptionUtils.getStackTrace(e));
                            return StartTxn.builder().statusCode(HttpStatus.INTERNAL_SERVER_ERROR.getCode())
                                    .error(PerfiosError.builder().message(e.getMessage()).build())
                                    .build();
                        }
                    } else {
                        return StartTxn.builder().statusCode(soapResp.getStatusCode())
                                .error(perfiosUtil.toPOJO(soapResp.getResponse(), PerfiosError.class))
                                .build();
                    }
                })).doOnError(err -> {
                    LOG.error("error while starting perfios txn: {} {}", perfiosReq.getApplicationId(), ExceptionUtils.getStackTrace(err));
                }).switchIfEmpty(Mono.defer(Mono::empty));
    }

    private Mono<UploadAllStmt> uploadAllStmt(Account account, String perfiosTxnId, String bucket) {
        List<CompletableFuture<UploadStmt>> futures = account.getFiles().stream()
                .map(file -> CompletableFuture.supplyAsync(() -> {
                    String password = file.isProtected() ? RSAUtil.encryptWithPublicKey(file.getPassword()) : "";
                    return perfiosUtil.upload(perfiosTxnId, file.getFileKey(), s3Util, password, bucket);
                })).collect(Collectors.toList());
        try {
            CompletableFuture<Void> allFutures = CompletableFuture.allOf(
                    futures.toArray(new CompletableFuture[futures.size()])
            );
            CompletableFuture<List<UploadStmt>> allCompletedFuture =
                    allFutures.thenApply(future -> futures.stream().map(CompletableFuture::join)
                            .collect(Collectors.toList()));

            return allCompletedFuture.thenApply(uploadStmts -> {
                int count = 0;
                AtomicReference<Mono<UploadAllStmt>> uploadAllStmt = new AtomicReference<>();
                for (UploadStmt uploadStmt : uploadStmts) {
                    if (uploadStmt.getStatusCode() == HttpStatus.OK.getCode() ||
                            uploadStmt.getStatusCode() == HttpStatus.ACCEPTED.getCode()) {
                        count++;
                    } else {
                        uploadAllStmt.set(Mono.just(UploadAllStmt.builder().statusCode(uploadStmt.getStatusCode())
                                .error(PerfiosUtil.getError(uploadStmt)).build()));
                        break;
                    }
                }

                if (count == uploadStmts.size()) {
                    uploadAllStmt.set(Mono.just(UploadAllStmt.builder().statusCode(HttpStatus.OK.getCode()).build()));
                }

                uploadStmts.stream().forEach(uploadStmt -> uploadStmt.getFile().delete());

                return uploadAllStmt.get();
            }).get();
        } catch (CompletionException | InterruptedException | ExecutionException e) {
            LOG.error("error while uploading bank statements to perfios {} {}", ExceptionUtils.getStackTrace(e), perfiosTxnId);
            return Mono.just(UploadAllStmt.builder().statusCode(HttpStatus.INTERNAL_SERVER_ERROR.getCode())
                    .error(PerfiosError.builder().message(e.getMessage()).build()).build());
        }
    }


    private Mono<UploadComplete> uploadComplete(String perfiosTxnId, String clientTxnId, TransactionLog txnLog) {
        LOG.info("uploading_complete task started {} {} {}", perfiosTxnId, clientTxnId, System.currentTimeMillis());

        Timer.Sample sample = Timer.start(MetricsUtil.registry);

        return perfiosUtil.getSOAPResponse(perfiosUtil.getPayloadRequest(Operation.UPLOAD_COMPLETE, perfiosTxnId, clientTxnId), soapUtil)
                .switchIfEmpty(Mono.defer(Mono::empty))
                .map(ExceptionUtil.wrapper(soapResp -> {
                    if (soapResp.getStatusCode() == HttpStatus.OK.getCode() ||
                            soapResp.getStatusCode() == HttpStatus.ACCEPTED.getCode()) {
                        txnUtil.updatePerfiosTxnStatus(txnLog, TransactionUtil.UPLOAD_COMPLETED);
                        LOG.info("uploading_complete task finished {} {} {}", perfiosTxnId, clientTxnId, System.currentTimeMillis());

                        sample.stop(MetricsUtil.registry.timer("bank_transcription.upload_complete.timer", "vendor", PerfiosUtil.PERFIOS));
                        return UploadComplete.builder().statusCode(soapResp.getStatusCode()).build();
                    } else {
                        return UploadComplete.builder().statusCode(soapResp.getStatusCode())
                                .error(MAPPER.readValue(soapResp.getResponse(), PerfiosError.class)).build();
                    }
                })).doOnError(err -> {
                    LOG.error("error while retrieving report: {} {} {}", ExceptionUtils.getStackTrace(err), perfiosTxnId, clientTxnId);
                }).switchIfEmpty(Mono.defer(Mono::empty));
    }

    public Mono<PerfiosResponse> pollForData(String perfiosTxnId, String clientTxnId, TransactionLog txnLog) {
        final int[] count = {1};
        AtomicReference<Mono<PerfiosResponse>> perfiosRespMono = new AtomicReference<>();
        do {
            checkStatus(perfiosTxnId, clientTxnId, txnLog).map(txnStatus -> {
                if (txnStatus != null && txnStatus.getStatusCode() == HttpStatus.OK.getCode() && txnStatus.getSuccess() != null) {
                    if (txnStatus.getSuccess().getPart().getStatus().equalsIgnoreCase(SUCCESS)
                            && txnStatus.getSuccess().getFiles().equalsIgnoreCase(AVAILABLE)
                            && txnStatus.getSuccess().getProcessing().equalsIgnoreCase(COMPLETED)) {

                        perfiosRespMono.set(retrieveReport(perfiosTxnId, clientTxnId, txnLog)
                                .map(retrieveReport -> {
                                    if (retrieveReport.getStatusCode() == HttpStatus.OK.getCode()) {
                                        return new PerfiosResponse(PerfiosData.builder()
                                                .perfiosTxnId(perfiosTxnId).clientTxnId(clientTxnId)
                                                .report(retrieveReport.getReport()).build());
                                    } else {
                                        PerfiosResponse perfiosResp = new PerfiosResponse(false, retrieveReport.getError().getMessage());
                                        perfiosResp.setErrorCode(retrieveReport.getError().getCode());
                                        perfiosResp.setStatementErrorCode(retrieveReport.getError().getStatementErrorCode());
                                        return perfiosResp;
                                    }
                                }));
                    } else if (txnStatus.getSuccess().getPart().getStatus().equalsIgnoreCase(PENDING)
                            && txnStatus.getSuccess().getFiles().equalsIgnoreCase(NOT_AVAILABLE)
                            && txnStatus.getSuccess().getProcessing().equalsIgnoreCase(PENDING)) {

                        perfiosRespMono.set(Mono.just(new PerfiosResponse(PerfiosData.builder()
                                .perfiosTxnId(perfiosTxnId).clientTxnId(clientTxnId).build())));
                    }
                    count[0] = numRetry;
                    LOG.info("pass count: {} {} {} {}", count[0], System.currentTimeMillis(), perfiosTxnId, clientTxnId);
                } else {
                    count[0]++;
                    LOG.info("fail count: {} {} {} {}", count[0], System.currentTimeMillis(), perfiosTxnId, clientTxnId);

                    PerfiosResponse perfiosResp = new PerfiosResponse(false, txnStatus.getError().getMessage());
                    perfiosResp.setErrorCode(txnStatus.getError().getCode());
                    perfiosResp.setStatementErrorCode(txnStatus.getError().getStatementErrorCode());

                    perfiosRespMono.set(Mono.just(perfiosResp));
                }
                return Mono.empty();
            }).block();

            if (count[0] < numRetry) {
                try {
                    Thread.sleep(timeout, 0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } while (count[0] < numRetry);
        return perfiosRespMono.get().switchIfEmpty(Mono.defer(Mono::empty));
    }


    public Mono<TxnStatus> checkStatus(String perfiosTxnId, String clientTxnId, TransactionLog txnLog) {
        LOG.info("check_status task started {} {} {}", perfiosTxnId, clientTxnId, System.currentTimeMillis());

        Timer.Sample sample = Timer.start(MetricsUtil.registry);

        return perfiosUtil.getSOAPResponse(perfiosUtil.getPayloadRequest(Operation.CHECK_STATUS, perfiosTxnId, clientTxnId), soapUtil)
                .map(ExceptionUtil.wrapper(soapResponse -> {
                    if (soapResponse.getStatusCode() == HttpStatus.OK.getCode()) {
                        try {
                            LOG.info("check_status task finished {} {} {}", perfiosTxnId, clientTxnId, System.currentTimeMillis());

                            sample.stop(MetricsUtil.registry.timer("bank_transcription.check_status.timer", "vendor", PerfiosUtil.PERFIOS));

                            TxnStatus txnStatus = TxnStatus.builder()
                                    .statusCode(soapResponse.getStatusCode())
                                    .success(perfiosUtil.toPOJO(soapResponse.getResponse(), TxnStatusSuccess.class))
                                    .build();

                            if (txnLog != null && txnLog.getId() != null) {
                                txnUtil.updatePerfiosTxnStatus(txnLog, txnStatus.getSuccess().getPart().getStatus().toUpperCase());
                            }
                            return txnStatus;
                        } catch (Exception e) {
                            LOG.error("error while checking status: {} {} {}", perfiosTxnId, clientTxnId, ExceptionUtils.getStackTrace(e));

                            return TxnStatus.builder()
                                    .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.getCode())
                                    .error(PerfiosError.builder().message(e.getMessage()).build())
                                    .build();
                        }
                    } else {
                        return TxnStatus.builder()
                                .statusCode(soapResponse.getStatusCode())
                                .error(perfiosUtil.toPOJO(soapResponse.getResponse(), PerfiosError.class))
                                .build();
                    }
                })).doOnError(err -> {
                    LOG.error("error while checking report status: {}", ExceptionUtils.getStackTrace(err));
                }).switchIfEmpty(Mono.defer(Mono::empty));
    }

    public Mono<RetrieveReport> retrieveReport(String perfiosTxnId, String clientTxnId, TransactionLog txnLog) {
        LOG.info("retrieve_report task started {} {} {}", perfiosTxnId, clientTxnId, System.currentTimeMillis());

        Timer.Sample sample = Timer.start(MetricsUtil.registry);

        return perfiosUtil.getSOAPResponse(perfiosUtil.getPayloadRequest(Operation.RETRIEVE_DATA, perfiosTxnId, clientTxnId), soapUtil)
                .map(ExceptionUtil.wrapper(soapResponse -> {
                    if (soapResponse.getStatusCode() == HttpStatus.OK.getCode()) {
                        try {
                            LOG.info("retrieve_report task finished {} {} {}", perfiosTxnId, clientTxnId, System.currentTimeMillis());

                            sample.stop(MetricsUtil.registry.timer("bank_transcription.retrieve_report.timer", "vendor", PerfiosUtil.PERFIOS));

                            RetrieveReport retrieveReport = RetrieveReport.builder()
                                    .statusCode(soapResponse.getStatusCode())
                                    .report(MAPPER.readValue(soapResponse.getResponse(), Report.class))
                                    .build();

                            if (txnLog != null && txnLog.getId() != null) {
                                txnLog.setOutput(MAPPER.writeValueAsString(retrieveReport.getReport()));
                                txnUtil.updatePerfiosTxnStatus(txnLog, TransactionUtil.COMPLETED);
                            }

                            return retrieveReport;
                        } catch (Exception e) {
                            LOG.error("error while retrieving report: {} {} {}", perfiosTxnId, clientTxnId, ExceptionUtils.getStackTrace(e));

                            return RetrieveReport.builder()
                                    .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.getCode())
                                    .error(PerfiosError.builder().message(e.getMessage()).build())
                                    .build();
                        }
                    } else {
                        return RetrieveReport.builder()
                                .statusCode(soapResponse.getStatusCode())
                                .error(MAPPER.readValue(soapResponse.getResponse(), PerfiosError.class))
                                .build();
                    }
                })).doOnError(err -> {
                    LOG.error("error while retrieving report: {} {} {}", perfiosTxnId, clientTxnId, ExceptionUtils.getStackTrace(err));
                }).switchIfEmpty(Mono.defer(Mono::empty));
    }
}

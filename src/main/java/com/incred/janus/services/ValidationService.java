package com.incred.janus.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.incred.janus.models.authbridge.client.ValidationRequest;
import com.incred.janus.models.authbridge.client.ValidationResponse;
import com.incred.janus.models.authbridge.vendor.AuthBridgeVendorRequest;
import com.incred.janus.models.authbridge.vendor.AuthBridgeVendorRequestParam;
import com.incred.janus.models.core.Response;
import com.incred.janus.models.karza.vendor.KarzaRequest;
import com.incred.janus.util.*;
import io.lettuce.core.SetArgs;
import io.lettuce.core.api.StatefulRedisConnection;
import io.micrometer.core.instrument.Timer;
import io.micronaut.context.annotation.Value;
import io.micronaut.core.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

import static com.incred.janus.core.ExceptionUtil.wrapper;
import static reactor.core.publisher.Mono.defer;
import static reactor.core.publisher.Mono.just;

@Singleton
public class ValidationService {

    private static final Logger log = LoggerFactory.getLogger(ValidationService.class);
    private final ObjectMapper objectMapper = new ObjectMapper();
    public final String appJson = "Application/json";
    public final long secondsInMonth = 60 * 60 * 24 * 30;
    @Inject
    ValidationUtil validationUtil;
    @Value("${authBridge.url}")
    String authBridgeUrl;
    @Value("${authBridge.username}")
    String username;
    @Value("${authBridge.password}")
    String password;
    @Value("${karza.url}")
    String karzaUrl;
    @Value("${karza.key}")
    String key;
    @Inject
    RestUtil client;
    @Inject
    StatefulRedisConnection<String, String> connection;
    public static final String AUTH_BRIDGE = "AUTH_BRIDGE";
    public static final String KARZA = "KARZA";
    public static final String CACHE_HIT = "CACHE_HIT";
    public static final String LOG = "LOG";
    @Inject
    TransactionUtil util;
    @Inject
    MetricsUtil metricsUtil;

    @Inject
    DynamoDBUtil dbUtil;


    public Mono<ValidationResponse> validate(@Valid ValidationRequest request) {
        return getFromCache(request)
                .switchIfEmpty(defer(() -> getFromDb(request).subscribeOn(Schedulers.elastic())))
                .switchIfEmpty(defer(() -> callVendor("primary", request)))
                .switchIfEmpty(defer(() -> callVendor("secondary", request)))
                .switchIfEmpty(just(new ValidationResponse(false, "Error processing request", null)))
                .map(response -> storeInCache(request, response))
                .map(response -> storeInDynamo(request, response))
                .onErrorResume(err -> just(new ValidationResponse(false, "Error processing request : "
                        + err.getMessage(), null))).log().retry(3);
    }

    public Mono<ValidationResponse> callVendor(String type, ValidationRequest request) {
        String vendor = validationUtil.getVendor(type);
        switch (vendor) {
            case "AuthBridge":
                return getFromAuthBridge(request)
                        .map(wrapper(response -> logTransaction(request, response, AUTH_BRIDGE)))
                        .subscribeOn(Schedulers.elastic());
            case "Karza":
                return getFromKarza(request)
                        .map(wrapper(response -> logTransaction(request, response, KARZA)))
                        .subscribeOn(Schedulers.elastic());
            default:
                return null;
        }
    }

    private Mono<ValidationResponse> getFromCache(@Valid ValidationRequest validationRequest) {
        try {
            MetricsUtil.init();
            initialise(validationRequest, "CACHE_HIT");
            Timer.Sample sample = Timer.start(MetricsUtil.registry);
            log.info("Fetching from cache for : " + validationRequest.getDocNumber());
            String cachedResult = connection.reactive()
                    .get(("validate:" + validationRequest.getDocType() + ":" + validationRequest
                            .getDocNumber()).toUpperCase()).subscribeOn(Schedulers.elastic())
                    .block();
            if (StringUtils.isNotEmpty(cachedResult)) {
                ValidationResponse validationResponse = objectMapper.readValue(cachedResult, ValidationResponse.class);
                validationResponse.setCacheHit(true);
                sample.stop(MetricsUtil.registry.timer("validation.timer", "vendor", CACHE_HIT));
                return Mono.just(validationResponse)
                        .map(wrapper(
                                response -> logTransaction(validationRequest, validationResponse, "CACHE_HIT")));
            }
        } catch (Exception e) {
            log.error("Unable to fetch from cache : " + validationRequest.getDocNumber());
        }
        return Mono.empty();
    }


    private ValidationResponse logTransaction(ValidationRequest validationRequest, ValidationResponse validationResponse, String vendor) throws JsonProcessingException {
        Timer.Sample sample = Timer.start(MetricsUtil.registry);
        validationResponse.setTransactionId(validationRequest.getTransactionId());
        initialise(validationRequest, vendor);
        util.logTransaction(validationRequest, validationResponse, vendor, "VALIDATION", "COMPLETED", 0L);
        sample.stop(MetricsUtil.registry.timer("validation.timer", "vendor", LOG));
        return validationResponse;
    }

    private Mono<ValidationResponse> getFromDb(@Valid ValidationRequest validationRequest) {
        try {
            MetricsUtil.init();
            Timer.Sample sample = Timer.start(MetricsUtil.registry);
            log.info("Fetching from db for : " + validationRequest.getDocNumber());
            String cachedResult = dbUtil.get(DynamoDBUtil.VALIDATION_PAN, "DOC_NO", validationRequest.getDocNumber());
            if (StringUtils.isNotEmpty(cachedResult)) {
                @SuppressWarnings("rawtypes") Map cachedResponse = objectMapper.readValue(cachedResult, Map.class);
                sample.stop(MetricsUtil.registry.timer("validation.timer", "vendor", CACHE_HIT));
                ValidationResponse validationResponse = objectMapper
                        .convertValue(cachedResponse.get("RESPONSE"), ValidationResponse.class);
                validationResponse.setCacheHit(true);
                return Mono.just(validationResponse)
                        .map(wrapper(response -> logTransaction(validationRequest, validationResponse, CACHE_HIT)));
            }
        } catch (Exception e) {
            log.error("Unable for fetch from DB : " + e.getMessage());
        }
        return Mono.empty();

    }

    private Mono<ValidationResponse> getFromAuthBridge(@Valid ValidationRequest validationRequest) {
        Timer.Sample sample = Timer.start(MetricsUtil.registry);
        log.info("Fetching from authBridge for : " + validationRequest.getDocNumber());
        Map<String, String> headers = Collections.singletonMap("username", username);
        return just(validationRequest)
                .map(request -> new AuthBridgeVendorRequestParam(request.getTransactionId(), request.getDocType(),
                        request.getDocNumber()))
                .map(wrapper(objectMapper::writeValueAsString))
                .map(paramString -> AesCbcCrypto.encrypt(password, RandomUtil.unique().substring(0, 16), paramString))
                .map(AuthBridgeVendorRequest::new)
                .map(wrapper(vRequest -> client.post(authBridgeUrl, vRequest, headers, appJson)
                        .subscribeOn(Schedulers.elastic())))
                .flatMap(Function.identity())
                .map(wrapper(restResponse -> {
                    sample.stop(MetricsUtil.registry.timer("validation.timer", "vendor", AUTH_BRIDGE));
                    return validationUtil.parseAuthBridgeResponse(restResponse, password);
                }))
                .filter(Response::getSuccess)
                .subscribeOn(Schedulers.elastic());
    }

    private Mono<ValidationResponse> getFromKarza(ValidationRequest validationRequest) {
        Timer.Sample sample = Timer.start(MetricsUtil.registry);
        log.info("Fetching from karza for : " + validationRequest.getDocNumber());
        Map<String, String> headers = Collections.singletonMap("x-karza-key", key);
        return just(validationRequest)
                .map(request -> new KarzaRequest(request.getDocNumber(), "Y"))
                .map(wrapper(karzaRequest -> client.post(karzaUrl, karzaRequest, headers, appJson)
                        .subscribeOn(Schedulers.elastic())))
                .flatMap(Function.identity())
                .map(wrapper(restResponse -> {
                    sample.stop(MetricsUtil.registry.timer("validation.timer", "vendor", KARZA));
                    return validationUtil.parseKarzaResponse(restResponse, validationRequest);
                }))
                .subscribeOn(Schedulers.elastic());
    }

    private ValidationResponse storeInCache(ValidationRequest request, ValidationResponse response) {
        try {
            if (response.getSuccess() && response.getError() == null && !response.isCacheHit()) {
                connection.async()
                        .set(("validate:" + request.getDocType() + ":" + request.getDocNumber()).toUpperCase(),
                                objectMapper.writeValueAsString(response), SetArgs.Builder.nx().ex(secondsInMonth));
            }
        } catch (Exception e) {
            log.error("Unable to write to cache : " + e.getMessage());
        }
        return response;
    }


    private ValidationResponse storeInDynamo(ValidationRequest request, ValidationResponse response) {
        try {
            log.info("Storing in Dynamo : " + request.getDocNumber());
            if (response.getSuccess() && response.getError() == null && !response.isCacheHit()) {
                dbUtil.put(DynamoDBUtil.VALIDATION_PAN, "DOC_NO", request.getDocNumber(),
                        Collections.singletonMap("RESPONSE", response));
            }
        } catch (Exception e) {
            log.error("Unable to write to DB : " + e.getMessage());
        }
        return response;
    }


    private synchronized void initialise(ValidationRequest validationRequest, String vendor) {
        try {
            String platform = validationRequest.getPlatform();
            String sender = validationRequest.getSender();
            log.info("Platform: " + platform + " Sender: " + sender + " PAN: " + validationRequest.getDocNumber());
            if (platform != null && sender != null) {
                metricsUtil.getMeterRegistry().counter(vendor, platform, sender).increment();
            } else {
                metricsUtil.getMeterRegistry().counter(vendor).increment();
            }

        } catch (Exception e) {
            log.error(e.getMessage() + "occurred in initialiseCounter method ");
        }
    }

}

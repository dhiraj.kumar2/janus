package com.incred.janus.core;

import com.incred.janus.models.core.Request;
import com.incred.janus.models.core.Response;
import com.incred.janus.models.core.TransactionLog;
import com.incred.janus.util.TransactionUtil;
import io.micronaut.aop.MethodInterceptor;
import io.micronaut.aop.MethodInvocationContext;
import lombok.SneakyThrows;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TransactionLogger implements MethodInterceptor<Object, Object> {

    @Inject
    TransactionUtil util;

    @SneakyThrows
    @Override
    public Object intercept(MethodInvocationContext<Object, Object> context) {
        String[] responses = context.getReturnType().getType().getName().toUpperCase().replace("RESPONSE", "")
                                    .split("\\.");
        String vendor = responses[responses.length - 1];
        Request request = null;
        for (Object parameterValue : context.getParameterValues()) {
            if (parameterValue.getClass().getName().contains("Request")) {
                request = (Request) parameterValue;
                break;
            }
        }
        if (request == null) {
            throw new IllegalArgumentException("Request is mandatory on @Log annotation");
        }
        TransactionLog transactionLog = util.initiateTransaction(request, vendor);
        Response response = null;
        Throwable throwable = null;
        try {
            response = (Response) context.proceed();
        } catch (Throwable ex) {
            throwable = ex;
            throw ex;
        } finally {
            if (response == null) {
                assert throwable != null;
                response = new Response(false, "Error occurred while processing request : " + throwable.getMessage());
            }
            util.updateTransaction(response, transactionLog);
        }
        return response;
    }
}

cd /var/www/html/janus
export ENV=PROD
mvn clean install -DskipTests
java -jar -Dmicronaut.environments=prod target/janus-0.1.jar
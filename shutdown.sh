#!/bin/bash

echo "Shutting down janus"
ps -ef | grep janus
echo -------------------------
ps -ef | grep janus | grep -v grep | awk '{print "kill -9",$2}' | sh
